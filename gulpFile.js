var gulp = require('gulp')
var spsave = require('gulp-spsave')
var watch = require('gulp-watch')
var cached = require('gulp-cached');

var rename = require('gulp-rename');

var coreOptions = {
    siteUrl: 'https://meralco.sharepoint.com/sites/cybersecYOUrity/',
    notification: false,
    // path to document library or in this case the master pages gallery
    folder: 'SiteAssets/build', 
    flatten: false

};
var creds = {
    username: 'v-pjslandayan@meralco.com.ph',
    password: 'L@ndayan0505972'
};

gulp.task('rename', function() {
    // rename via string
    return gulp.src('./build/index.html')
        .pipe(rename('InfoSecOnline.aspx'))
        .pipe(gulp.dest('./build')); // ./dist/main/text/ciao/goodbye.md
});

gulp.task('update-homepage', function() {
    // rename via string
    coreOptions.folder = 'SitePages'
    return gulp.src('./build/InfoSecOnline.aspx')
        .pipe(cached('spFiles'))
        .pipe(spsave(coreOptions, creds)); 
});

gulp.task('upload-build', function() {
    // runs the spsave gulp command on only files the have 
    // changed in the cached files
    coreOptions.folder = 'SiteAssets/build'
    return gulp.src('build/**')
        .pipe(cached('spFiles'))
        .pipe(spsave(coreOptions, creds));     
});

gulp.task('deploy', gulp.series('rename','update-homepage','upload-build'));

gulp.task('default', function() {
    // create an initial in-memory cache of files
    gulp.src('build/**')
    .pipe(cached('spFiles'));
    
    // watch the src folder for any changes of the files
    gulp.watch([ './build/**' ], [ 'spdefault' ]);
});