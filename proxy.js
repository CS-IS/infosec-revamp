const RestProxy = require('sp-rest-proxy');
const settings = {
    configPath : './infosec.cred.json',
    port : 4323,
    staticRoot : './static'
};

const restProxy = new RestProxy(settings);
restProxy.serveProxy();