import 'react-app-polyfill/stable';
import 'react-app-polyfill/ie11';

import React from 'react';
import AppRouter from './router/AppRouter';
import './styles/styles.scss';



function App() {
  return (
    <React.Fragment>
      <AppRouter />
    </React.Fragment>
  );
}

export default App;