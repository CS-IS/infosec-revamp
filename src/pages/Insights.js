import React from 'react';
import List from '../components/List';

class Insights extends React.Component {
    render() {
        return (
            <List 
            title={"Insights"}
            list={"Insights"}
            />
        );
    }
}

export default Insights;