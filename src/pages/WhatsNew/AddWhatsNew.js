import React from 'react';

import InnerHeader from '../../components/InnerHeader';
import ViewControl from '../../components/ViewControl';
import Footer from '../../components/Footer';
import WhatsNewForm from '../../components/WhatsNewForm';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import '../../styles/_aboutpage.scss'
import '../../styles/_global.scss';

export default class AddWhatsNew extends React.Component {
    render() {
        return (
            <div className="about-page">
                <div className="about-page-container">
                    <Container fluid="true" className={"no-padding"}>
                        <Row noGutters="true">
                            <Col lg={2}>
                                <ViewControl />
                            </Col>
                            <Col lg={10}>
                                <InnerHeader text={"What's New > Add New"}/>
                                <WhatsNewForm mode={"Add"} category={"Admin"} module={"What's New"}/>
                            </Col>
                        </Row>
                    </Container>
                </div>

                <Footer />
            </div>
        );
    }
}