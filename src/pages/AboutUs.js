import React from 'react';

import AboutUsInnerHeader from '../components/AboutUsInnerHeader';
import AboutUsView from '../components/AboutUsView';
import AboutUsEdit from '../components/AboutUsEdit';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Row from 'react-bootstrap/Row';
import ViewControl from '../components/ViewControl';

import { getAboutUsContent, saveAboutUsContent, attachFilesToListitem, deleteAllAttachments, LIST_ATTACHMENTS_PREFIX } from '../sp-data/lists/about-us';

import '../styles/_aboutpage.scss'
import '../styles/_global.scss';

class AboutUs extends React.Component {

    state = {
        inEditMode: false,
        Body: "",
        Id: null,
        Caption1: "",
        Caption2: "",
        Caption3: "",
        Caption4: "",
        Caption5: "",
        Image1: "",
        Image2: "",
        Image3: "",
        Image4: "",
        Image5: "",
        dataFlag: false,
        viewContentStatus : false,
        itemRetrieved : false
    }

    handleModeToggle = (props) => {
        this.setState(() => ({
            inEditMode: props
        }));
    }

    handleSave = () => {
        this.setState((prev) => {
            return {
                dataFlag: true
            }
        });
    }

    handleStartSave = (props) => {
        const fileInfo = [];
        let itemId = props.Id ? props.Id : null;
        const fileNames = [];
        let itemRetrieved;
        if(props.itemRetrieved){
            itemRetrieved = props.itemRetrieved;
            delete props.itemRetrieved;
        }
        

        for (let i = 1; i <= 5; i++) {
            const Image = `Image${i}`;
            const File = `File${i}`;

            if (!props[File]) {
                delete props[File];
            } else {
                props[Image] = `${Image}===${props[File].name}`;
                fileInfo.push({
                    name: `${Image}===${props[File].name}`,
                    content: props[File]
                });
                delete props[File];
            }

            if (props[Image]) {
                props[Image] = props[Image].substring(props[Image].lastIndexOf('/') + 1);
            }

            if (itemId) {
                if (this.state[Image]) {
                    const fileName = this.state[Image].substring(this.state[Image].lastIndexOf('/') + 1);
                    if (fileName !== props[Image]) {
                        fileNames.push(fileName);
                    }
                }
            }
        }

        if (fileNames.length > 0) {
            deleteAllAttachments(itemId, fileNames).then(() => {
                saveAboutUsContent(props.Id, props)
                    .then((result) => {
                        if (result.data.Id) {
                            const {
                                Body, Id,
                                Image1, Image2, Image3, Image4, Image5,
                                Caption1, Caption2, Caption3, Caption4, Caption5
                            } = result.data;

                            attachFilesToListitem(Id, fileInfo).then((result) => {
                                this.setState(() => ({
                                    ...this.state,
                                    Body, Id,
                                    Image1, Image2, Image3, Image4, Image5,
                                    Caption1, Caption2, Caption3, Caption4, Caption5,
                                    inEditMode: false,
                                    dataFlag: false,
                                    itemRetrieved
                                }));
                            });
                        } else {
                            attachFilesToListitem(itemId, fileInfo).then((result) => {
                                debugger;
                                this.setState((prevSate) => {
                                    return {
                                        ...props,
                                        inEditMode: false,
                                        dataFlag: false,
                                        Image1: props.Image1 ? `${LIST_ATTACHMENTS_PREFIX}/${itemId}/${props.Image1}` : undefined,
                                        Image2: props.Image2 ? `${LIST_ATTACHMENTS_PREFIX}/${itemId}/${props.Image2}` : undefined,
                                        Image3: props.Image3 ? `${LIST_ATTACHMENTS_PREFIX}/${itemId}/${props.Image3}` : undefined,
                                        Image4: props.Image4 ? `${LIST_ATTACHMENTS_PREFIX}/${itemId}/${props.Image4}` : undefined,
                                        Image5: props.Image5 ? `${LIST_ATTACHMENTS_PREFIX}/${itemId}/${props.Image5}` : undefined,
                                        itemRetrieved
                                    }
                                })
                            });
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            });
        } else {
            saveAboutUsContent(props.Id, props)
                .then((result) => {
                    if (result.data.Id) {
                        const {
                            Body, Id,
                            Image1,
                            Image2,
                            Image3,
                            Image4,
                            Image5,
                            Caption1, Caption2, Caption3, Caption4, Caption5
                        } = result.data;

                        const listItem = {
                            inEditMode: false,
                            dataFlag: false,
                            Body, Id,
                            Image1,
                            Image2,
                            Image3,
                            Image4,
                            Image5,
                            Caption1, Caption2, Caption3, Caption4, Caption5,
                            itemRetrieved
                        };

                        if (listItem.Image1) {
                            listItem.Image1 = `${LIST_ATTACHMENTS_PREFIX}/${Id}/${listItem.Image1}`;
                        } else {
                            delete listItem.Image1;
                        }

                        if (listItem.Image2) {
                            listItem.Image2 = `${LIST_ATTACHMENTS_PREFIX}/${Id}/${listItem.Image2}`;
                        } else {
                            delete listItem.Image2;
                        }

                        if (listItem.Image3) {
                            listItem.Image3 = `${LIST_ATTACHMENTS_PREFIX}/${Id}/${listItem.Image3}`;
                        } else {
                            delete listItem.Image3;
                        }

                        if (listItem.Image4) {
                            listItem.Image4 = `${LIST_ATTACHMENTS_PREFIX}/${Id}/${listItem.Image4}`;
                        } else {
                            delete listItem.Image4;
                        }

                        if (listItem.Image5) {
                            listItem.Image5 = `${LIST_ATTACHMENTS_PREFIX}/${Id}/${listItem.Image5}`;
                        } else {
                            delete listItem.Image5;
                        }

                        attachFilesToListitem(Id, fileInfo).then((result) => {
                            this.setState(() => (listItem));
                        });

                    } else {
                        attachFilesToListitem(itemId, fileInfo).then((result) => {
                            this.setState((prevSate) => {
                                return {
                                    ...props,
                                    inEditMode: false,
                                    dataFlag: false,
                                    Image1: props.Image1 ? `${LIST_ATTACHMENTS_PREFIX}/${itemId}/${props.Image1}` : undefined,
                                    Image2: props.Image2 ? `${LIST_ATTACHMENTS_PREFIX}/${itemId}/${props.Image2}` : undefined,
                                    Image3: props.Image3 ? `${LIST_ATTACHMENTS_PREFIX}/${itemId}/${props.Image3}` : undefined,
                                    Image4: props.Image4 ? `${LIST_ATTACHMENTS_PREFIX}/${itemId}/${props.Image4}` : undefined,
                                    Image5: props.Image5 ? `${LIST_ATTACHMENTS_PREFIX}/${itemId}/${props.Image5}` : undefined,
                                    itemRetrieved
                                }
                            })
                        });
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    }

    handleFailedValidation = () => {
        console.log("Turn off save");
        this.setState((prev) => {
            return {
                dataFlag: false
            }
        });
    }

    componentDidMount() {
        getAboutUsContent()
            .then((result) => {
                if (result.length > 0) {
                    let {
                        Body, Id,
                        Image1, Image2, Image3, Image4, Image5,
                        Caption1, Caption2, Caption3, Caption4, Caption5
                    } = result[0];

                    Image1 = Image1 ? `${LIST_ATTACHMENTS_PREFIX}/${Id}/${Image1}` : undefined;
                    Image2 = Image2 ? `${LIST_ATTACHMENTS_PREFIX}/${Id}/${Image2}` : undefined;
                    Image3 = Image3 ? `${LIST_ATTACHMENTS_PREFIX}/${Id}/${Image3}` : undefined;
                    Image4 = Image4 ? `${LIST_ATTACHMENTS_PREFIX}/${Id}/${Image4}` : undefined;
                    Image5 = Image5 ? `${LIST_ATTACHMENTS_PREFIX}/${Id}/${Image5}` : undefined;

                    this.setState(() => ({
                        Body, Id,
                        Image1,
                        Image2,
                        Image3,
                        Image4,
                        Image5,
                        Caption1, Caption2, Caption3, Caption4, Caption5,
                        viewContentStatus : true,
                        itemRetrieved : true
                    }));
                }else{
                    this.setState(() => ({
                        viewContentStatus : true
                    }));
                }

            })
            .catch((err) => {
                console.log(err);
            });
    }

    render() {
        return (
            <div className="about-page">
                <div className="about-page-container">
                    <Container fluid="true" className={"no-padding"}>
                        <Row noGutters="true">
                            <Col lg={2}>
                                <ViewControl isDisabled={true} />
                            </Col>
                            <Col lg={10}>
                                <AboutUsInnerHeader
                                    text={"What is Information Security?"}
                                    edit={this.state.inEditMode}
                                    handleSave={this.handleSave}
                                    handleModeToggle={this.handleModeToggle}
                                />
                                
                                <div className="about-page-content">
                                    {
                                        <PerfectScrollbar>
                                        {!this.state.inEditMode ?
                                            <AboutUsView {...this.state} /> :
                                            <AboutUsEdit
                                                content={this.state}
                                                handleStartSave={this.handleStartSave}
                                                handleFailedValidation={this.handleFailedValidation}
                                            />}
                                        </PerfectScrollbar>
                                    }
                                </div>
                                
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}

export default AboutUs;