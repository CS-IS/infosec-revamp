import React from 'react';
import List from '../../components/UndertakingList';

class Undertaking extends React.Component {
    render() {
        return (
            <List 
            title={"Undertaking"}
            list={"Undertaking"}
            export={true}
            />
        );
    }
}

export default Undertaking;