import React from 'react';

import UndertakingInnerHeader from '../../components/UndertakingInnerHeader';
import ViewControl from '../../components/ViewControl';
import Footer from '../../components/Footer';
import ViewItem from '../../components/UndertakingView';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import '../../styles/_aboutpage.scss'
import '../../styles/_global.scss';
import '../../styles/_viewform.scss';

export default class ViewUndertaking extends React.Component {
    state = {
        Body: "",
        dataFlag: false,
        viewContentStatus: false,
        itemRetrieved: false
    }

    render() {
        return (
            <div className="about-page">
                <div className="about-page-container">
                    <Container fluid="true" className={"no-padding"}>
                        <Row noGutters="true">
                            <Col lg={2}>
                                <ViewControl isDisabled={true}/>
                            </Col>
                            <Col lg={10}>
                                <UndertakingInnerHeader
                                    text={"Policies > InfoSec Forms > View Item"}
                                    id={this.props.match.params.id}
                                    history={this.props.history}
                                />
                                <ViewItem id={this.props.match.params.id} list={"Undertaking"} />
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}