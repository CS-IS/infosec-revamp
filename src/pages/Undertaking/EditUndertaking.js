import React from 'react';

import InnerHeader from '../../components/InnerHeader';
import ViewControl from '../../components/ViewControl';
import Footer from '../../components/Footer';
import UndertakingForm from '../../components/UndertakingForm';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import '../../styles/_aboutpage.scss'
import '../../styles/_global.scss';

export default class AddUndertaking extends React.Component {
    render() {
        return (
            <div className="about-page">
                <div className="about-page-container">
                    <Container fluid="true" className={"no-padding"}>
                        <Row noGutters="true">
                            <Col lg={2}>
                                <ViewControl isDisabled={true}/>
                            </Col>
                            <Col lg={10}>
                                <InnerHeader text={"Policies > InfoSec Forms > Edit Item"} />
                                <UndertakingForm mode={"Edit"} category={"Admin"} id={this.props.match.params.id} />
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}