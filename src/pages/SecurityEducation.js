import React from 'react';
import List from '../components/List';

class SecurityEducation extends React.Component {
    render() {
        console.log(this.props);
        return (
            <List 
            title={"Security Education"}
            category={this.props.match.params.category}
            list={"Security Education"}
            />
        );
    }
}

export default SecurityEducation;