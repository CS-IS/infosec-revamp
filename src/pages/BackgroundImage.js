import React from 'react';

import AddBackgroundImage from '../components/AddBackgroundImage';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import InnerHeader from '../components/BackgroundImageInnerHeader';
import Row from 'react-bootstrap/Row';
import Swal from 'sweetalert2';
import ViewControl from '../components/ViewControl';

import { getHomepagePictures, saveHomepagePictures, LIST_ATTACHMENTS_PREFIX } from '../sp-data/lists/homepage-pictures';

import '../styles/_global.scss';
import '../styles/_adminpage.scss';

class BackgroundImage extends React.Component {

    state = {
        getFormDataFlag : false,
        images : [],
        imagesSent : false,
        dataSaving : false
    }

    componentDidMount(){
        getHomepagePictures().then((res) => {
            const images = [];
            res.forEach(({Id, ImageOrder, ImageUrl}) => {
                images.push({
                    Id,
                    ImageOrder,
                    ImageUrl : `${LIST_ATTACHMENTS_PREFIX}/${Id}/${ImageUrl}`
                });
            });
            this.setState(() => ({
                images,
                imagesSent : true
            }));
        });
    }

    handleSave = () => {
        this.setState(() => ({
            getFormDataFlag : true,
            dataSaving : true
        }));
    }

    handleDiscard = () => {
        this.resetSend();
        getHomepagePictures().then((res) => {
            console.log(res);
            const images = [];
            res.forEach(({Id, ImageOrder, ImageUrl}) => {
                images.push({
                    Id,
                    ImageOrder,
                    ImageUrl : `${LIST_ATTACHMENTS_PREFIX}/${Id}/${ImageUrl}`
                });
            });
            this.setState(() => ({
                images,
                imagesSent : true,
                dataSaving : false
            }));
        });
    }

    resetSend = () => {
        this.setState(() => ({
            imagesSent : false,
            getFormDataFlag : false
        }));
    }

    handleStartSPSave = (props) => {
        const stateIds = this.state.images.map((item) => item.Id);
        const newIds = props.map((item) => item.Id);
        console.log("state", stateIds);
        console.log("props sent", newIds);
        const idsToDelete = stateIds.filter((item) => !newIds.includes(item));

        if(props.length > 0 || idsToDelete.length > 0){
            saveHomepagePictures(props, idsToDelete).then(setTimeout((res) => {
                console.log("COMPLETE");
                getHomepagePictures().then((res) => {
                    const images = [];
                    res.forEach(({Id, ImageOrder, ImageUrl}) => {
                        images.push({
                            Id,
                            ImageOrder,
                            ImageUrl : `${LIST_ATTACHMENTS_PREFIX}/${Id}/${ImageUrl}`
                        });
                    });
                    this.setState(() => ({
                        images,
                        imagesSent : true,
                        dataSaving : false
                    }));
                    Swal.fire({
                        title: 'Changes has been reflected',
                        type: 'success',
                        confirmButtonColor: '#3cb371',
                        confirmButtonText: 'Ok'
                    });
                });
            },3000));
        }else{
            this.setState(() => ({
                images : [],
                imagesSent : true,
                dataSaving : false
            }));
            Swal.fire({
                title: 'Changes has been reflected',
                type: 'success',
                confirmButtonColor: '#3cb371',
                confirmButtonText: 'Ok'
            });
        }  
    }

    

    render() {
        return (
            <div className="admin-page admin-page--edit-bg">
                <Container fluid="true" className={"no-padding"}>
                    <Row noGutters="true">
                        <Col lg={2}>
                            <ViewControl isDisabled={true}/>
                        </Col>
                        <Col lg={10}>
                            <InnerHeader
                            text={"Background Images"} 
                            handleSave = {this.handleSave}
                            handleDiscard = {this.handleDiscard}
                            />
                            <AddBackgroundImage
                            images = {this.state.images}
                            imagesSent = {this.state.imagesSent}
                            getFormDataFlag = {this.state.getFormDataFlag}
                            handleStartSPSave = {this.handleStartSPSave}
                            dataSaving = {this.state.dataSaving}
                            resetSend = {this.resetSend}
                            />
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default BackgroundImage;