import React from 'react';
import List from '../components/List';

class Policies extends React.Component {
    render() {
        return (
            <List 
            title={"Policies"}
            category={this.props.match.params.category}
            list={"Policies"}
            />
        );
    }
}

export default Policies;