import React from 'react';

import BackgroundSlider from '../components/BackgroundSlider';
import ReactTooltip from 'react-tooltip';
import { Tooltip } from 'react-tippy';
import HomepageCarousel from '../components/HomepageCarousel';
import { links } from '../sp-data/sp-links';
import { withRouter } from "react-router";

import '../styles/_homepage.scss';
import 'react-tippy/dist/tippy.css';

class Index extends React.Component {

    render() {
        return (
            <div className="homepage-container">
                <div className="homepage-overlay" />
                <div className="background-container">
                    <BackgroundSlider />
                </div>
                <div className="homepage-content">
                    <h1>Welcome to InfoSec Online</h1>
                    <div className="homepage-carousel">
                        <h5>What's New?</h5>
                        <HomepageCarousel history={this.props.history} />
                    </div>
                    <div className="homepage-contact">
                        <p>Contact us at</p>
                        <Tooltip
                            title={`<b>Choose a category: </b><br>
                            <a class="contact-us-link" target="_blank"
                            href="${links.home.mail.cyberSecAssessment}">
                            Cybersecurity Assessment
                            </a><br>
                            <a class="contact-us-link" target="_blank"
                            href="${links.home.mail.cyberSecConsultancy}">
                            Cybersecurity Consultancy
                            </a>`}
                            position="bottom"
                            interactive="true"
                            trigger="click"
                        >
                            <a>
                                <img src={require("../assets/images/envelope.svg")} alt="email-logo" />
                            </a>
                        </Tooltip>

                        <a href={`${links.home.workplace}`} target="_blank"><img src={require("../assets/images/workplace-logo.png")} alt="workplace-logo" /></a>
                        <a data-tip="8944">
                            <img src={require("../assets/images/phone-call.svg")} alt="phonecall-logo" />
                        </a>
                        <ReactTooltip place="top" type="light" effect="solid" />
                        <a className="homepage__btn-report"
                            target="_blank"
                            href={`${links.home.reportIncident}`}>
                            <img src={require("../assets/images/chat.svg")} alt="message-logo" />
                            Report an Incident
                            </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Index);