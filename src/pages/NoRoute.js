import { initializeIcons } from '@uifabric/icons';
import React, { Component } from 'react';
import '../styles/error.scss';
import {config} from '../environment/environment';
import { Link } from 'react-router-dom';
initializeIcons(undefined, { disableWarnings: true });

class ErrorPage extends Component {
    constructor(props, state) {
        super(props);
    }

    render() {
        return (
            <div className="formError">
                <div className="error-container">
                    <h1>404</h1>
                    <p className="return"><Link to='/' className='nav-link'>Return to Home Page</Link></p>
                </div>
            </div>
        );
    }
}

export default ErrorPage;
