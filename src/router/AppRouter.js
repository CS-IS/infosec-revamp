import React from 'react';
import { Switch, BrowserRouter, Route, Redirect } from "react-router-dom";
import Header from '../components/Header';

import Index from "../pages/Index";
import AboutUs from "../pages/AboutUs";
import SecurityEducation from '../pages/SecurityEducation';
import Policies from '../pages/Policies'
import Insights from '../pages/Insights'
import AddForm from "../components/AddForm";
import BackgroundImage from '../pages/BackgroundImage';
import EditForm from '../components/EditForm';
import EditWhatsNew from '../pages/WhatsNew/EditWhatsNew'; 
import ViewWhatsNew from '../pages/WhatsNew/ViewWhatsNew';
import ViewAllWhatsNew from '../pages/WhatsNew/ViewAllWhatsNew';
import Undertaking from '../pages/Undertaking/ViewAllUndertaking';
import AddUndertaking from '../pages/Undertaking/AddUndertaking';
import ViewUndertaking from '../pages/Undertaking/ViewUndertaking';
import EditUndertaking from '../pages/Undertaking/EditUndertaking';
import ViewForm from '../components/ViewForm';
import NoRoute from '../pages/NoRoute';

import { config } from "../environment/environment";

const AppRouter = () => (
 
   <BrowserRouter basename={`${config.serverRelativeURL}SitePages/InfoSecOnline.aspx`}>
      <Header />
      <Switch>        
        <Route exact path="/" component={Index} />        
        <Route exact path="/AboutUs" component={AboutUs} />
        <Route exact path="/WhatsNew" component={ViewAllWhatsNew} />
        <Route exact path="/WhatsNew/EditItem/:id" component={EditWhatsNew} />
        <Route exact path="/WhatsNew/ViewItem/:id" component={ViewWhatsNew} />

        <Route exact path="/Undertaking" component={Undertaking} />
        <Route exact path="/Undertaking/NewItem" component={AddUndertaking} />
        <Route exact path="/Undertaking/EditItem/:id" component={EditUndertaking} />
        <Route exact path="/Undertaking/ViewItem/:id" component={ViewUndertaking} />
        
        <Route exact path="/Security Education/:category" component={SecurityEducation} />
        <Route exact path="/Policies/:category" component={Policies} />
        <Route exact path="/:list/:category/NewItem" component={AddForm} />
        <Route exact path="/:list/:category/ViewItem/:id" component={ViewForm} />
        <Route exact path="/:list/ViewItem/:id" component={ViewForm} />
        <Route exact path="/:list/:category/EditItem/:id" component={EditForm} />
        <Route exact path="/:list/EditItem/:id" component={EditForm} />
        <Route exact path="/:list/NewItem" component={AddForm} />
        <Route exact path="/Insights" component={Insights} />
        <Route exact path="/Settings/Admin/AddBackgroundImage" component={BackgroundImage} />

        <Route exact path="/404" component={ NoRoute } />
        <Redirect to="/404" />
      </Switch>
  </BrowserRouter>
);

export default AppRouter;