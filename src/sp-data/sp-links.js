import { config } from "../environment/environment";

const links = {
 header : {
  siteUsage : `https://meralco.sharepoint.com${config.serverRelativeURL}_layouts/15/Reporting.aspx?Category=AnalyticsSite`,
  oldSite : "https://meralco.sharepoint.com/sites/infoseconline/SitePages/HomePage.aspx"
 },
 home : {
  mail : {
   cyberSecAssessment : "https://outlook.office.com/owa/?path=%2fmail%2faction%2fcompose#to=meralco.cybersecurity%40meralco.com.ph&subject=Cybersecurity+Assessment",
   cyberSecConsultancy : "https://outlook.office.com/owa/?path=%2fmail%2faction%2fcompose#to=meralco.cybersecurity%40meralco.com.ph&subject=Cybersecurity+Consultancy"
  },
  workplace : "https://meralco.workplace.com/profile.php?id=100016209798766&fref=ts&epa=SEARCH_BOX",
  reportIncident : "https://outlook.office.com/owa/?path=%2fmail%2faction%2fcompose#to=meralco.cybersecurity%40meralco.com.ph&subject=Report+Security+Incident"
 },
 undertaking : {
  acknowledgements : `https://meralco.sharepoint.com${config.serverRelativeURL}Lists/Acknowledgements/AllItems.aspx`
 }
}

export { links };