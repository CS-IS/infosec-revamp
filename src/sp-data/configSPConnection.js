import { sp } from '@pnp/sp';
import { config } from "../environment/environment";

sp.setup({
  sp: {
    headers: {
      Accept: "application/json;odata=verbose",
    },
    baseUrl: config.url
  }
});

export default sp;