import sp from './configSPConnection';

const isAdmin = async () => {
 let isAdmin = false;
 const currentUserGroups = await sp.web.currentUser.select('groups/Title').expand('groups').get();
 currentUserGroups.Groups.results.forEach(element => {
  if(element.Title === "Administrators"){
   isAdmin = true;
  }
 });
 return isAdmin;
}

const currentUser = async () => {
    const dataNeeded = {
        name : "",
        office : ""
    };
    const curUser = await sp.web.currentUser.get();
    const user = await sp.web.ensureUser(curUser.Title).then((r)=>r);
    dataNeeded.name = curUser.Title;
    const profile = await sp.profiles.getPropertiesFor(user.data.LoginName);
    if(profile.UserProfileProperties.results.length > 0){
        const department = profile.UserProfileProperties.results.find(o => o.Key == "Department");
        dataNeeded.office = department.Value;
    }
    return dataNeeded;
}

export { isAdmin, currentUser };