import sp from '../configSPConnection';
import { config } from "../../environment/environment";

const LIST_NAME = "About Us";
const LIST_ATTACHMENTS_PREFIX = `${config.url}/Lists/AboutUs/Attachments`;

const getAboutUsContent = () => {
    return sp.web.lists.getByTitle(LIST_NAME).items.orderBy("ID", true).top(1).get();
}

const saveAboutUsContent = (id, content) => {
    delete content.Id;
    content.Image1 = !content.Image1 ? content.Image1 = "" : content.Image1;
    content.Image2 = !content.Image2 ? content.Image2 = "" : content.Image2;
    content.Image3 = !content.Image3 ? content.Image3 = "" : content.Image3;
    content.Image4 = !content.Image4 ? content.Image4 = "" : content.Image4;
    content.Image5 = !content.Image5 ? content.Image5 = "" : content.Image5;
    if (id) {
        debugger;
        return sp.web.lists.getByTitle(LIST_NAME).items.getById(id).update({
            ...content
        });
    } else {
        return sp.web.lists.getByTitle(LIST_NAME).items.add({
            ...content
        });
    }
}

const deleteAllAttachments = (id, filenames) => {
    const list = sp.web.lists.getByTitle(LIST_NAME);
    return list.items.getById(id).attachmentFiles.deleteMultiple(...filenames);
}

const attachFilesToListitem = (id, fileInfo) => {
    return sp.web.lists.getByTitle(LIST_NAME).items.getById(id).attachmentFiles.addMultiple(fileInfo);
}

const getListItemAttachments = (id) => {
    const listitem = sp.web.lists.getByTitle(LIST_NAME).items.getById(id);
    return listitem.attachmentFiles.get();
}

export { getAboutUsContent, saveAboutUsContent, attachFilesToListitem, deleteAllAttachments, getListItemAttachments, LIST_ATTACHMENTS_PREFIX };