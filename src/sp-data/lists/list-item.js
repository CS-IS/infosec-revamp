import sp from '../configSPConnection';
import { config } from '../../environment/environment';
// import { Web } from '@pnp/sp';
import { ConsoleListener, Web, Logger, LogLevel, ODataRaw } from "sp-pnp-js";
const addListItems = (list, category, values) => {
    return sp.web.lists.getByTitle(list.replace("'","")).items.add({
        Title: values.title,
        DateReleased: values.startDate,
        Details: values.body,
        ItemType: values.selectedType,
        Category : category,
        VideoLink: values.videoEmbed
    });
}
const updateListItems = (list, itemId, values) => {
    return sp.web.lists.getByTitle(list).items.getById(itemId).update({
        Title: values.title,
        DateReleased: values.startDate,
        Details: values.body,
        ItemType: values.selectedType,
        VideoLink: values.videoEmbed
    });
}
const deleteAttachments = (list, itemId, completeURL)=>{
    let filename = completeURL.substring(completeURL.lastIndexOf('/')+1);
    let url  = `/${config.serverRelativeURL}${list.replace(" ","").replace("'","")}Attachments/${itemId}/${filename}`;
    return sp.web.getFileByServerRelativePath(url).recycle();
}
const updateListItemsThumbnail = (list, itemId, fileName) => {
    const completeURL = `${config.meralcoUrl}/${config.serverRelativeURL}${list.replace(" ","").replace("'","")}Attachments/${itemId}/${fileName}`;
    return sp.web.lists.getByTitle(list).items.getById(itemId).update({
        Thumbnail : completeURL
    });
}
const deleteListItem = (list, itemId) => {
    return sp.web.lists.getByTitle(list).items.getById(itemId).delete();
}
const deleteFolder = (list, itemId) => {
    let web = new Web(config.url);
    const completeURL = config.serverRelativeURL + list.replace(" ","").replace("'","") + 'Attachments/' + itemId;
    return web.getFolderByServerRelativeUrl(completeURL).delete();
}
const addFile = (list, values, ID, fileType) => {
    const serverRelativeURL = config.serverRelativeURL;
    const folderID = ID;
    const file = values;
    const completeURL = serverRelativeURL + list.replace(" ","").replace("'","") + 'Attachments/' + folderID;
    let web = new Web(config.url);
    if(file.size < 10485760){
        return web.getFolderByServerRelativeUrl(completeURL).files.add(encodeURIComponent(file.name).replace(/'/g, '%27%27'), file, true).then(f => {
            f.file.getItem().then(item => {
                item.update({
                    FileType: fileType,
                });
            });
        }); 
    }else{
        return web.getFolderByServerRelativeUrl(completeURL).files.addChunked(encodeURIComponent(file.name).replace(/'/g, '%27%27'), file, data => {
            Logger.log({ data: data, level: LogLevel.Verbose, message: "progress" });
        }, true).then(f => {
            return web.getFolderByServerRelativeUrl(`/${f.file._url}`).getItem().then(item => {
                item.update({
                    FileType: fileType,
                });
            });
        });
    }
    // you can adjust this number to control what size files are uploaded in chunks
    // if (file.size <= 10485760) {
    //     // small upload
    //     return sp.web.getFolderByServerRelativeUrl(serverRelativeURL + LIBRARY_NAME + '/' + folderID).files.add(file.name, file, true).then(f => {
    //         f.file.getItem().then(item => {
    //             item.update({
    //                 FileType: fileType,
    //             });
    //         });
    //     });
    // } else {
    //     // large upload
    //     return sp.web.getFolderByServerRelativeUrl(serverRelativeURL + LIBRARY_NAME + '/' + folderID).files.addChunked(file.name, file, data => {}, true).then(f => {
    //         f.file.getItem().then(item => {
    //             item.update({
    //                 FileType: fileType,
    //             });
    //         });
    //     });  
    // }
}

const updateUndertaking = (list, itemId, data) => {
    return sp.web.lists.getByTitle(list).items.getById(itemId).update({
        ...data  
    });
}

const createFolder = (list, value) => {
    const folderName = value;
    return sp.web.folders.getByName(`${list.replace(" ","").replace("'","")}Attachments`).folders.add(folderName);
}

export { addListItems, addFile, createFolder, deleteListItem, deleteFolder, updateListItems, updateListItemsThumbnail, deleteAttachments, updateUndertaking };