import sp from '../configSPConnection';
import { Web } from '@pnp/sp';
import { config } from "../../environment/environment";

const LIST_NAME = "Background Images";
const LIST_ATTACHMENTS_PREFIX = `${config.url}/Lists/BackgroundImages/Attachments`;

const getHomepagePictures = () => {
    return sp.web.lists.getByTitle(LIST_NAME).items.select("ImageOrder", "ImageUrl", "Id", "Attachments").filter("ImageOrder ne null").orderBy("ImageOrder", true).top(5).get();
}

const saveHomepagePictures = async (images, idsToDelete) => {
    const web = new Web(config.url);
    let batch = web.createBatch();

    if (idsToDelete.length > 0) {
        idsToDelete.forEach(id => {
            sp.web.lists.getByTitle(LIST_NAME).items.getById(id).inBatch(batch).delete().then(() => { console.log("DELETED") });
        });
    }

    if (images.length > 0) {
        for (let i = 0; images.length > i; i++) {
            if (images[i].Id) {
                const id = images[i].Id;
                let file = undefined;

                if (images[i].file) {
                    file = images[i].file
                    images[i].ImageUrl = images[i].file.name;
                    delete images[i].file;
                } else {
                    images[i].ImageUrl = images[i].ImageUrl.substring(images[i].ImageUrl.lastIndexOf('/')).replace('/', '');
                }

                delete images[i].Id;

                const updateStatus = await web.lists.getByTitle(LIST_NAME).items.inBatch(batch).getById(id).update(images[i]).then(async (res) => {
                    if (file) {
                        let attachments = await web.lists.getByTitle(LIST_NAME).items.getById(id).attachmentFiles.get().then(att => att).catch(err => console.log(err));
                        const deleteStatus = await web.lists.getByTitle(LIST_NAME).items.getById(id).attachmentFiles.deleteMultiple(attachments[0].FileName).then(() => { console.log("deleted") });
                        const attachStatus = await web.lists.getByTitle(LIST_NAME).items.getById(id).attachmentFiles.add(images[i].ImageUrl, file).then(() => {
                            console.log("Attached");
                        }).catch(err => {
                                console.log("File Attach Failed");
                        });
                    }
                }).catch(err => console.log(err));

                /* const updateStatus = await web.lists.getByTitle(LIST_NAME).items.inBatch(batch).getById(id).update(images[i]);

                if (file) {
                    let attachments = await web.lists.getByTitle(LIST_NAME).items.getById(id).attachmentFiles.get().then(att => att).catch(err => console.log(err));
                    const deleteStatus = await web.lists.getByTitle(LIST_NAME).items.inBatch(batch).getById(id).attachmentFiles.deleteMultiple(attachments[0].FileName).then(() => { console.log("deleted") });
                    const attachStatus = await web.lists.getByTitle(LIST_NAME).items.getById(id).attachmentFiles.inBatch(batch).add(images[i].ImageUrl, file).then(() => {
                        console.log("Attached");
                    }).catch(err => {
                        console.log("File Attach Failed");
                    });
                } */

            } else {
                let file = undefined;
                if (images[i].file) {
                    file = images[i].file;
                    images[i].ImageUrl = images[i].file.name;

                }
                delete images[i].file;
                delete images[i].id;

                web.lists.getByTitle(LIST_NAME).items.inBatch(batch).add(images[i]).then((result) => {
                    if(file){
                        const listitem = web.lists.getByTitle(LIST_NAME).items.getById(result.data.Id);
                        listitem.attachmentFiles.inBatch(batch).add(file.name, file)
                            .then(() => {
                                console.log("Attached");
                            })
                            .catch(err => {
                                console.log("File Attach Failed");
                            });
                    }                    
                });

                /* const listItem = await web.lists.getByTitle(LIST_NAME).items.inBatch(batch).add(images[i]);
                if (file) {
                    const listitem1 = await web.lists.getByTitle(LIST_NAME).items.getById(result.data.Id);
                    listitem1.attachmentFiles.inBatch(batch).add(file.name, file)
                        .then(() => {
                            console.log("Attached");
                        })
                        .catch(err => {
                            console.log("File Attach Failed");
                        });

                } */
            }
        }
    }
    return batch.execute();
}

const deleteHomepagePictures = (idsToDelete) => {
    const web = new Web(config.url);
    let batch = web.createBatch();
    idsToDelete.forEach(id => {
        sp.web.lists.getByTitle(LIST_NAME).items.getById(id).inBatch(batch).delete().then(() => { console.log("DELETED") });
    });

    return batch.execute();
}

export { getHomepagePictures, saveHomepagePictures, deleteHomepagePictures, LIST_ATTACHMENTS_PREFIX };