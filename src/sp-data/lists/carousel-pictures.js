import sp from '../configSPConnection';

const LIST_NAME = "Carousel Pictures";

const getCarouselPictures = () => {
    return sp.web.lists.getByTitle(LIST_NAME).items.filter("Order0 ne null").orderBy("Order0", true).top(5).get();
    
}

export default getCarouselPictures;