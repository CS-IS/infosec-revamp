import sp from '../configSPConnection';
import { config } from '../../environment/environment';

const getListContent = (listname, category) => {
    const encodedList = encodeURIComponent(listname);
    const encodedCategory = encodeURIComponent(category);
    if (category) {
        return sp.web.lists.getByTitle(listname).items.top(9).filter(`Category eq '${encodedCategory}'`).orderBy("DateReleased", false).getPaged();
    } else if (listname === "Undertaking") {
        return sp.web.lists.getByTitle(listname).items.top(9).orderBy("ID", false).getPaged();
    }
    else {
        return sp.web.lists.getByTitle(listname).items.top(9).orderBy("DateReleased", false).getPaged();
    }
}

const getItemById = (id, listname) => {
    return sp.web.lists.getByTitle(listname).items.getById(id).get();
}

const updateItemViews = (listname, id, data) => {
    let itemCount = 0;
    if (data) {
        itemCount = parseInt(data);
    }
    console.log(itemCount);
    return sp.web.lists.getByTitle(listname).items.getById(id).update({
        Views: itemCount + 1
    });
}

const addItemToList = (listname, data) => {
    console.log(data);
    return sp.web.lists.getByTitle(listname).items.add({
        ...data
    });
}

// const getFiles = (serverRelativeURL) => {
//     return sp.web.getFolderByServerRelativeUrl(serverRelativeURL).getItem().then(item => {
//         console.log(item);
//     });
// }

const getFilesInsideFolder = (list, folderUrl) => {
    const serverRelativeURL = config.serverRelativeURL;
    const folderID = folderUrl;
    const completeURL = serverRelativeURL + list.replace(" ", "") + 'Attachments/' + folderID;

    return sp.web.getFolderByServerRelativeUrl(completeURL).expand('Files/ListItemAllFields').select('Title,FileType')              // Fields to retrieve
        .get();
}

const getCountIfMaximum = (listname) => {
    let count = 0;
    return sp.web.lists.getByTitle(listname).items.get();
}

const getFilesForSlider = async () => {

    //Gets Item Ids needed to get necessary files
    const list = "Whats New";
    const idArray = [];
    const listItems = await getListContent(list).then(res => res.results);
    listItems.forEach((item) => {
        idArray.push(item.ID);
    });

    //Gets Files With the Ids
    const listAttachmeLibrary = "WhatsNewAttachments/";
    const serverRelativeURL = config.serverRelativeURL;
    const files = [];


    for (let i = 0; i < idArray.length; i++) {
        const completeURL = serverRelativeURL + listAttachmeLibrary + idArray[i];
        const itemData = await sp.web.getFolderByServerRelativeUrl(completeURL).expand('Files/ListItemAllFields').select('Title,FileType')
            .get().then((item) => {
                if (item.Files.results.length === 1) {
                    return item.Files.results[0].ServerRelativeUrl;
                } else if (item.Files.results.length === 2) {
                    let toReturn = undefined;
                    item.Files.results.forEach((item) => {
                        if (item.ListItemAllFields.FileType === "Thumbnail") {
                            toReturn = item.ServerRelativeUrl;
                        }
                    });
                    return toReturn;
                }
            });

        if (itemData) {
            files.push({
                id: idArray[i],
                url: itemData
            });
        }

    }
    return files;
}

const filterListContent = (listname, category, title, year) => {
    let filter = ""

    if(listname !== "Undertaking"){
        if (category) {
            filter = "Category eq '" + encodeURIComponent(category) + "' ";
        }
    
        if (year !== "") {
            var first = new Date(parseInt(year), 0, 1);
            let lastday = new Date(parseInt(year), 11, 31);
    
            if(filter !== ""){
                filter += 'and (DateReleased ge datetime\'' + first.toISOString() + '\') and (DateReleased le datetime\'' + lastday.toISOString() + '\')';
            }else{
                filter += ' (DateReleased ge datetime\'' + first.toISOString() + '\') and (DateReleased le datetime\'' + lastday.toISOString() + '\')';
            }       
    
        }
    }
    

    if (title !== "") {
        if(filter !== ""){
            filter += "and substringof('" + encodeURIComponent(title).replace(/'/g, '%27%27') + "',Title)" ;
        }else{
            filter += "substringof('" + encodeURIComponent(title).replace(/'/g, '%27%27') + "',Title)" ;
        }            
    }

    if(listname === "Undertaking"){
        return sp.web.lists.getByTitle(listname).items.top(9).filter(filter).orderBy("Created", false).getPaged();
    }else{
        return sp.web.lists.getByTitle(listname).items.top(9).filter(filter).orderBy("DateReleased", false).getPaged();
    }   
    
}

const getAcknowledgedUndertaking = (listname, currentUser, id) => {
    const encodedList = encodeURIComponent(listname);
    return sp.web.lists.getByTitle(listname).items.filter(`Title eq '${currentUser}' and UndertakingId eq '${id}'`).get();
}

const checkIfUndertakingExisting = (listname, title) => {
    const encodedList = encodeURIComponent(listname);
    if (listname==="Undertaking") {
        return sp.web.lists.getByTitle(listname).items.filter(`Title eq '${title}'`).get();
    } else {
        return sp.web.lists.getByTitle(listname).items.filter(`UndertakingId eq '${title}'`).get();
    }
}

export { getListContent, getItemById, addItemToList, filterListContent, getFilesInsideFolder, getCountIfMaximum, updateItemViews, getFilesForSlider, getAcknowledgedUndertaking, checkIfUndertakingExisting };
