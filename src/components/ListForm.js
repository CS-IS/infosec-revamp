import React from 'react';

import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import DatePicker from "react-datepicker";
import { Editor } from '@tinymce/tinymce-react';
import Form from 'react-bootstrap/Form';
import Loader from './Loader';
import Row from 'react-bootstrap/Row';
import Swal from 'sweetalert2';
import { tinymce } from '../../node_modules/react-tinymce/lib/components/TinyMCE';

import { withRouter } from "react-router";

import "react-datepicker/dist/react-datepicker.css";
import '../styles/_forms.scss';
import '../styles/_global.scss';
import '../styles/_aboutpage.scss';

class ListForm extends React.Component {
    fileType = ["Text", "PDF", "Image", "PowerPoint", "Video"];
    errorMessage = "This field is required.";

    isInEdit = this.props.location.pathname.includes("EditItem");

    state = {
        title: "",
        body: "",
        selectedType: "Text",
        details: false,
        attachment: false,
        thumbnail: false,
        listAttachments: {
            fileAttachment: null,
            thumbnailAttachment: null,
        },
        listAttachmentsLink: {
            fileAttachmentLink: "",
            thumbnailAttachmentLink: "",
        },
        video: false,
        videoEmbed: "",
        startDate: new Date(),
        module: this.props.list || "",
        category: this.props.category || "",
        dataSaving: false,
        dataRetrieval: false,
        titleError: "",
        detailsError: "",
        thumbnailsError: "",
        attachmentError: "",
        videoError: "",
        thumbnailLink: "",

        thumbnailFileName: ""
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.data && this.props.data) {
            if (prevProps.data.dataRetrieval !== this.props.data.dataRetrieval && this.props.data.dataRetrieval === true) {
                this.changeFileType(this.props.data.ItemType);
                this.setState(() => ({
                    title: this.props.data.title,
                    body: this.props.data.details,
                    category: this.props.data.category,
                    startDate: this.props.data.dateReleased,
                    thumbnail: this.props.data.thumbnail,
                    videoEmbed: this.props.data.videoEmbed,
                    selectedType: this.props.data.ItemType,
                    listAttachmentsLink: this.props.data.listAttachmentsLink,
                    dataRetrieval: true
                }))
            }
        }
    }

    changeFileType(filetype) {
        document.getElementById("fileAttachment").value = "";
        document.getElementById("fileThumbnail").value = "";

        const { listAttachments } = this.state;
        switch (filetype) {
            case "Text":
                this.setState({
                    selectedType: filetype,
                    details: true,
                    attachment: false,
                    thumbnail: true,
                    video: false,

                    body: "",
                    listAttachments: {
                        fileAttachment: null,
                        thumbnailAttachment: null,
                    },
                    videoEmbed: "",
                    fileName: "",
                    fileAttachment: "",

                    titleError: null,
                    detailsError: null,
                    thumbnailsError: null,
                    attachmentError: null,
                    videoError: null
                });
                break;

            case "PDF":
                this.setState({
                    selectedType: filetype,
                    details: false,
                    attachment: true,
                    thumbnail: true,
                    video: false,

                    body: "",
                    listAttachments: {
                        fileAttachment: null,
                        thumbnailAttachment: null,
                    },
                    videoEmbed: "",
                    fileName: "",
                    fileAttachment: "",

                    titleError: null,
                    detailsError: null,
                    thumbnailsError: null,
                    attachmentError: null,
                    videoError: null
                });
                break;

            case "PowerPoint":
                this.setState({
                    selectedType: filetype,
                    details: false,
                    attachment: true,
                    thumbnail: true,
                    video: false,

                    body: "",
                    listAttachments: {
                        fileAttachment: null,
                        thumbnailAttachment: null,
                    },
                    videoEmbed: "",
                    fileName: "",
                    fileAttachment: "",

                    titleError: null,
                    detailsError: null,
                    thumbnailsError: null,
                    attachmentError: null,
                    videoError: null
                });
                break;

            case "Image":
                this.setState({
                    selectedType: filetype,
                    details: true,
                    attachment: false,
                    thumbnail: true,
                    video: false,

                    body: "",
                    listAttachments: {
                        fileAttachment: null,
                        thumbnailAttachment: null,
                    },
                    videoEmbed: "",
                    fileName: "",
                    fileAttachment: "",

                    titleError: null,
                    detailsError: null,
                    thumbnailsError: null,
                    attachmentError: null,
                    videoError: null
                });
                break;

            case "Video":
                this.setState({
                    selectedType: filetype,
                    details: false,
                    attachment: false,
                    thumbnail: true,
                    video: true,

                    body: "",
                    listAttachments: {
                        fileAttachment: null,
                        thumbnailAttachment: null,
                    },
                    fileName: "",
                    fileAttachment: "",
                    
                    titleError: null,
                    detailsError: null,
                    thumbnailsError: null,
                    attachmentError: null,
                    videoError: null
                });
                break;

            default:
                this.setState({
                    ...this.state
                });
                break;
        }
    }

    validateForm = () => {
        const { selectedType,
            titleError,
            detailsError,
            attachmentError,
            thumbnailsError,
            videoError,

            title,
            body,
            listAttachments,
            videoEmbed
        } = this.state;

        const elementError = ["title", "details", "thumbnails", "attachment", "video"];
        elementError.map((element) => {
            let item = element + "Error";
            this.setState({
                [item]: ""
            });
        })

        let isValid = false;
        let invalidElements = [];


        switch (selectedType) {
            case "Text":
                if (!title.trim()) { invalidElements.push("title"); }
                if (!body) { invalidElements.push("details"); }
                if (!listAttachments.thumbnailAttachment && !this.isInEdit) { invalidElements.push("thumbnails"); }

                if (title && body && listAttachments.thumbnailAttachment) {
                    isValid = true;
                }
                break;
            case "PDF":
                if (!title) { invalidElements.push("title"); }
                if (!listAttachments.thumbnailAttachment && !this.isInEdit) { invalidElements.push("thumbnails"); }
                if (!listAttachments.fileAttachment && !this.isInEdit) { invalidElements.push("attachment"); }

                if (title && body && listAttachments.fileAttachment) {
                    isValid = true;
                }
                break;
            case "Image":
                if (!title.trim()) { invalidElements.push("title"); }
                if (!body) { invalidElements.push("details"); }
                if (!listAttachments.thumbnailAttachment && !this.isInEdit) { invalidElements.push("thumbnails"); }

                if (title && body && listAttachments.thumbnailAttachment) {
                    isValid = true;
                }
                break;
            case "PowerPoint":
                if (!title.trim()) { invalidElements.push("title"); }
                if (!listAttachments.fileAttachment && !this.isInEdit) { invalidElements.push("attachment"); }
                if (!listAttachments.thumbnailAttachment && !this.isInEdit) { invalidElements.push("thumbnails"); }

                if (title && listAttachments.fileAttachment && listAttachments.thumbnailAttachment) {
                    isValid = true;
                }
                break;
            case "Video":
                if (!title.trim()) { invalidElements.push("title"); }
                if (!videoEmbed.trim()) { invalidElements.push("video"); }
                if (!listAttachments.thumbnailAttachment && !this.isInEdit) { invalidElements.push("thumbnails"); }

                if (title && videoEmbed && listAttachments.thumbnailAttachment) {
                    isValid = true;
                }
                break;
        }
        return invalidElements;
    }

    handleDropdownChange = (e) => {
        this.changeFileType(e.target.value);
    }

    handleTitleChange = (e) => {
        this.setState({ title: e.target.value });
    }

    validateTitle = () => {
        const { title } = this.state;
        this.setState({
            titleError:
            title.trim().length < 1 ? this.errorMessage : null
        });
    }

    handleDateChange = (date) => { this.setState({ startDate: date }); }

    handleDetailsChange = (value) => {
        console.log(value);
        this.setState(() => ({ body: value }));
    }

    handleChangeThumbnailName = (e) => {
        switch (e.target.name) {
            case 'selectedFile':
                if (e.target.files.length > 0) {
                    this.setState({ fileName: e.target.files[0].name });
                }
                break;
            default:
                this.setState({ [e.target.name]: e.target.value });
        }

        const attachment = { ...this.state.listAttachments };

        if (e.target.files.length > 0) {
            const { selectedType } = this.state;
            const file = e.target.files[0];
            const mbSize = file.size / 1024 / 1024;
            const fileType = file.type === 'image/jpeg' || file.type === 'image/png';

            if (!fileType) {
                Swal.fire({
                    title: 'Selected file is not valid',
                    type: 'error',
                    confirmButtonColor: '#3cb371',
                    cancelButtonColor: '#dc143c',
                    confirmButtonText: 'Ok'
                });
                e.target.value = null;
                this.setState(() => ({
                    fileName: "",
                    listAttachments: {
                        thumbnailAttachment: null,
                        fileAttachment: attachment.fileAttachment,
                    }
                }));
                return;
            }

            if (selectedType === "Image") {
                if (mbSize > 20) {
                    Swal.fire({
                        title: 'Maximum attachment size is up to 20MB only',
                        type: 'error',
                        confirmButtonColor: '#3cb371',
                        cancelButtonColor: '#dc143c',
                        confirmButtonText: 'Ok'
                    });
                    e.target.value = null;
                    this.setState(() => ({
                        fileName: "",
                        listAttachments: {
                            thumbnailAttachment: null,
                            fileAttachment: attachment.fileAttachment,
                        }
                    }));
                    return;
                }
            }
            else if (selectedType !== "Image") {
                if (mbSize > 1) {
                    Swal.fire({
                        title: 'Maximum attachment size is up to 1MB only',
                        type: 'error',
                        confirmButtonColor: '#3cb371',
                        cancelButtonColor: '#dc143c',
                        confirmButtonText: 'Ok'
                    });
                    e.target.value = null;
                    this.setState(() => ({
                        fileName: "",
                        listAttachments: {
                            thumbnailAttachment: null,
                            fileAttachment: attachment.fileAttachment,
                        }

                    }));
                    return;
                }
            }

            attachment.thumbnailAttachment = file;
            this.setState(() => ({
                listAttachments: attachment
            }));
        }
        else {
            return false;
        }
    };

    validateThumbnail = () => {
        const fileCount = document.getElementById("fileThumbnail").files.length;
        this.setState({
            thumbnailsError: fileCount === 0 ? this.errorMessage : null
        });
    }

    handleVideo = (e) => {
        this.setState({ videoEmbed: e.target.value });
    }

    validateVideo = () => {
        const { videoEmbed } = this.state;
        this.setState({
            videoError:
            videoEmbed.trim().length < 1 ? this.errorMessage : null
        });
    }

    handleChangeFile = (e) => {
        switch (e.target.name) {
            case 'selectedAttachment':
                if (e.target.files.length > 0) {
                    this.setState({ fileAttachment: e.target.files[0].name });
                }
                break;
            default:
                this.setState({ [e.target.name]: e.target.value });
        }

        const attachment = { ...this.state.listAttachments };

        if (e.target.files.length > 0) {
            const { selectedType } = this.state;
            const file = e.target.files[0];
            const mbSize = file.size / 1024 / 1024;
            let fileType;

            switch (selectedType) {
                case "PDF":
                    fileType = file.type === 'application/pdf';
                    break;
                case "PowerPoint":
                    fileType = file.type === 'application/vnd.openxmlformats-officedocument.presentationml.presentation' || file.type === "application/vnd.ms-powerpoint";
                    break;
                case "Image":
                    fileType = file.type === 'image/jpeg' || file.type === 'image/png';
                    break;
                default:
                    break;
            }

            if (!fileType) {
                Swal.fire({
                    title: 'Selected file is not valid',
                    type: 'error',
                    confirmButtonColor: '#3cb371',
                    cancelButtonColor: '#dc143c',
                    confirmButtonText: 'Ok'
                });
                e.target.value = null;
                this.setState(() => ({
                    fileAttachment: "",
                    listAttachments: {
                        fileAttachment: null,
                        thumbnailAttachment: attachment.thumbnailAttachment
                    }
                }));
                return;
            }

            if (mbSize > 20) {
                Swal.fire({
                    title: 'Maximum attachment size is up to 20MB only',
                    type: 'error',
                    confirmButtonColor: '#3cb371',
                    cancelButtonColor: '#dc143c',
                    confirmButtonText: 'Ok'
                });
                e.target.value = null;
                attachment.fileAttachment = null;
                this.setState(() => ({
                    fileAttachment: "",
                    listAttachments: attachment
                }));

                return;
            }

            attachment.fileAttachment = file;
            this.setState(() => ({
                listAttachments: attachment
            }));
        }
        else {
            return false;
        }
    }

    handleDiscardClick = () => {
        this.props.history.goBack();
    }

    handleAddClick = () => {
        const invalidElements = this.validateForm();

        if (invalidElements.length > 0) {
            invalidElements.map((element) => {
                let states = element + "Error";
                this.setState({
                    [states]: this.errorMessage
                })
            });
        }

        else {
            Swal.fire({
                title: (this.isInEdit ? "Are you sure you want to save changes?" : 'Are you sure you want to add this item?'),
                showCancelButton: true,
                confirmButtonColor: '#3cb371',
                cancelButtonColor: '#dc143c',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then((result) => {

                if (result.value) {
                    this.setState(() => ({
                        dataSaving: true
                    }));
                    this.props.handleAddClick(this.state.module, this.state.category, this.state);
                }
            });
        }
    }

    componentDidMount() {
        console.log(this.props);
        if (this.props.location.pathname.includes("NewItem")) {
            this.setState(() => ({ dataRetrieval: true }));
        }
        if (this.state.category === "Awareness") {
            this.changeFileType("Image");
        } else {
            this.changeFileType(this.state.selectedType);
        }
    }

    getFile = (e) => {
        const mbSize = e.target.files[0].size / 1024 / 1024;
        if (e.target.id === "fileThumbnail") {
            if (mbSize > 1) {
                alert("Maximum attachment size is up to 1MB only");
                e.target.value = null;
            }
        }
        else {
            if (mbSize > 20) {
                alert("Maximum attachment size is up to 20MB only");
                e.target.value = null;
            }
        }
    }

    disableTyping = (e) => {
        e.preventDefault();
        return false;
    }

    render() {
        const { module, category } = this.state;
        console.log(this.state);

        // changing the default value of file type label to the selected file name 
        const { fileName, fileAttachment } = this.state;

        let attachmentFile = null;
        let file = null;

        file = fileName
            ? (<span>File Selected - {fileName}</span>)
            : (<span>Upload a file</span>);

        attachmentFile = fileAttachment
            ? (<span>File Selected - {fileAttachment}</span>)
            : (<span>Upload a file</span>);

        return (
            <React.Fragment>
                <Container className={"form-background with-padding"} id="customScrollbar">
                    {
                        this.state.dataSaving &&
                        <div style={{ height: '75%', width: '100%', position: 'absolute' }}>
                            <Loader />
                        </div>
                    }

                    <Row>
                        <Col md={4}>
                            <Form.Group>
                                <Form.Label>Title *</Form.Label>
                                <Form.Control id="txtTitle"
                                    placeholder="Title"
                                    maxLength="255"
                                    value={this.state.title}
                                    onChange={this.handleTitleChange}
                                    className={`form-control ${this.state.titleError ? 'is-invalid' : ''}`}
                                    required />
                                <div className='invalid-feedback'>{this.state.titleError}</div>
                            </Form.Group>
                        </Col>

                        <Col md={4}>
                            <Form.Group>
                                <Form.Label>Date Released</Form.Label>
                                <br />
                                <DatePicker id="txtDateReleased"
                                    selected={this.state.startDate}
                                    onChange={this.handleDateChange}
                                    onKeyDown={this.disableTyping}
                                    className={`form-control ${this.state.dateError ? 'is-invalid' : ''}`}
                                />
                                <div className='invalid-feedback'>{this.state.dateError}</div>
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={4}>
                            <Form.Group>
                                <Form.Label>Select a type *</Form.Label>
                                <Form.Control id="ddnFileType"
                                    as="select"
                                    onChange={this.handleDropdownChange}
                                    value={this.state.selectedType}
                                    required
                                    disabled={this.isInEdit ? true : false}>
                                    {
                                        (() => {
                                            switch (module) {
                                                case "Policies":
                                                    this.fileType = this.fileType.filter(i => i !== "Image").filter(v => v !== "Video");
                                                    return (this.fileType.map((file) => {
                                                        return <option
                                                            value={file}
                                                            key={file}>{file}</option>
                                                    }));
                                                default:
                                                    if (category === "Awareness") {
                                                        return <option
                                                            value="Image"
                                                            key="Image">Image</option>
                                                    } else {
                                                        return (this.fileType.map((file) => {
                                                            return <option
                                                                value={file}
                                                                key={file}>{file}</option>
                                                        }));
                                                    }
                                            }
                                        })()
                                    }
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row style={{ display: this.state.details ? 'block' : 'none' }}>
                        <Col md={9}>
                            <Form.Group>
                                <Form.Label>Details * </Form.Label>
                                {this.state.dataRetrieval && <Editor
                                    id="txtBody"
                                    apiKey="API_KEY"
                                    init={{
                                        plugins: 'link table',
                                        height: 270,
                                        statusbar: false
                                    }}
                                    onEditorChange={this.handleDetailsChange}
                                    /* initialValue={this.state.body} */
                                    value={this.state.body}
                                    className={`${this.state.detailsError ? 'is-invalid' : ''}`}
                                />}
                                <div className='invalid-feedback'
                                    style={{ display: this.state.detailsError ? 'block' : 'none' }}>
                                    {this.state.detailsError}
                                </div>

                            </Form.Group>
                        </Col>
                    </Row>

                    <Row style={{ display: this.state.attachment ? 'block' : 'none' }}>
                        <Col md={4}>
                            <Form.Group>
                                <Form.Label>File Attachment</Form.Label>

                                {this.state.listAttachmentsLink.fileAttachmentLink != "" ? <div>
                                    {<a href={`${this.state.listAttachmentsLink.fileAttachmentLink}?web=1`} target="_blank">{this.props.data.originalFilename}</a>}
                                    <br /><br /></div> : <div></div>}

                                <input
                                    type="file"
                                    className="custom-input-file"
                                    id="fileAttachment"
                                    name="selectedAttachment"
                                    required={this.state.attachment}
                                    onChange={(event) => this.handleChangeFile(event)}
                                />
                                <label htmlFor="fileAttachment">{attachmentFile}</label>

                                <div className='invalid-feedback'
                                    style={{ display: this.state.attachmentError ? 'block' : 'none' }}>
                                    {this.state.attachmentError}
                                </div>
                                <small className="form-text text-muted">Note: File must be 20MB or below</small>
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row style={{ display: this.state.video ? 'block' : 'none' }}>
                        <Col md={7}>
                            <Form.Group>
                                <Form.Label>Video Embed Link</Form.Label>
                                <textarea id="txtVideoLink"
                                    type="text"
                                    className="form-control"
                                    placeholder="Video Embed Link"
                                    onChange={this.handleVideo}
                                    value={this.state.videoEmbed}
                                    rows={5}
                                    pattern="^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$" />
                                <div className='invalid-feedback'
                                    style={{ display: this.state.video ? 'block' : 'none' }}>
                                    {this.state.videoError}
                                </div>
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row style={{ display: this.state.thumbnail ? 'block' : 'none' }}>
                        <Col md={4}>
                            <Form.Group>
                                <Form.Label>{this.state.selectedType === "Image" ? "Upload Photo" : "Thumbnail"}</Form.Label>
                                {this.state.listAttachmentsLink.thumbnailAttachmentLink != "" ? <div>
                                    {<img src={this.state.listAttachmentsLink.thumbnailAttachmentLink} width={"200px"} height={"200px"} />}
                                    <br /><br /></div> : <div></div>}

                                <input
                                    type="file"
                                    className="custom-input-file"
                                    id="fileThumbnail"
                                    name="selectedFile"
                                    required={this.state.thumbnail}
                                    onChange={(event) => this.handleChangeThumbnailName(event)}
                                />
                                <label htmlFor="fileThumbnail">{file}</label>

                                <div className='invalid-feedback'
                                    style={{ display: this.state.thumbnailsError ? 'block' : 'none' }}>
                                    {this.state.thumbnailsError}
                                </div>
                                <small className="form-text text-muted">Note: Image must be {this.state.selectedType === "Image" ? "20MB" : "1MB"} or below</small>
                            </Form.Group>
                        </Col>
                    </Row>

                    <br />
                    <ButtonToolbar>
                        <Button type="submit" onClick={this.handleAddClick} className="btn-infosec btn-infosec--save">Save</Button>
                        <Button onClick={this.handleDiscardClick} className="btn-infosec btn-infosec--delete sample-debug">Discard</Button>
                    </ButtonToolbar>
                </Container>
            </React.Fragment>
        )
    }
}

export default withRouter(ListForm);