import React from 'react';

import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Loader from './Loader';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Row from 'react-bootstrap/Row';
import Swal from 'sweetalert2';

import '../styles/_forms.scss';

class AddBackgroundImage extends React.Component {
    defaultImage = {
        ImageUrl: require("../assets/images/defaultuploaderimg.png"),
        file: undefined,
        ImageOrder: undefined,
        id: null
    }

    state = {
        images: []
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.imagesSent != this.props.imagesSent && this.props.imagesSent === true) {
            if(this.props.images.length > 0){
                this.setState(() => ({
                    images: [...this.props.images]
                }));
            }else{
                this.setState(() => ({
                    images: [this.defaultImage]
                }));
            }            
        }

        if (this.props.getFormDataFlag) {
            const imagesToSave = this.state.images.filter((item, index) => {
                if(index === 0){
                    return item;
                }else{
                    return item.ImageUrl !== this.defaultImage.ImageUrl;
                }               
            });

            imagesToSave.forEach((item, index) => {
                item.ImageOrder = index
            });

            this.props.handleStartSPSave(imagesToSave);
            this.props.resetSend();
        }
    }

    changeSelectedPhoto = (e) => {
        const err = false;
        const file = e.target.files[0];
        const mbSize = file.size / 1024 / 1024;
        const fileType = file.type === 'image/jpeg' || file.type === 'image/png';
        console.log(e.target.files[0]);

        if (!fileType) {
            Swal.fire({
                title: 'Selected file is not valid',
                type: 'error',
                confirmButtonColor: '#3cb371',
                cancelButtonColor: '#dc143c',
                confirmButtonText: 'Ok'
            });
            e.target.value = null;
            return;
        }

        if (mbSize > 5) {
            Swal.fire({
                title: 'Maximum attachment size is up to 5MB only',
                type: 'error',
                confirmButtonColor: '#3cb371',
                cancelButtonColor: '#dc143c',
                confirmButtonText: 'Ok'
            });
            e.target.value = null;
            return;
        }

        const id = e.target.id.replace('fileImg', '');
        const updatedArr = [...this.state.images];
        const updatedObj = { ...updatedArr[id] };
        updatedObj.ImageUrl = URL.createObjectURL(file);
        updatedObj.file = file;
        updatedArr.splice(id, 1, updatedObj);
        this.setState(() => ({
            images: updatedArr
        }));
    }

    handleAddImage = () => {
        const newImage = { ...this.defaultImage };
        newImage.ImageOrder = this.state.images.length;

        this.setState(() => {
            return {
                images: [...this.state.images, newImage]
            }
        });
    }

    handleRemoveImage = (e) => {
        const id = e.target.id.replace('fileRemove', '');
        const updatedArr = [...this.state.images];
        updatedArr.splice(id, 1);
        Swal.fire({
            title: 'Are you sure you want to delete this item?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3cb371',
            cancelButtonColor: '#dc143c',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
          }).then((result) => {
            if (result.value) {              
                this.setState(() => {
                    return {
                        images: [...updatedArr]
                    }
                });
            }
          });
    }

    render() {
        return (
            <div className="form--edit-bg">
                {(this.props.dataSaving || this.state.images.length === 0) && <Loader />}
                <PerfectScrollbar>
                <Container style={{paddingBottom: '25px'}}>
                    <Row>
                        <Col lg={12} className="content">
                            <table>
                                <tbody>
                                    {
                                        this.state.images.map((item, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td >
                                                        <Form.Group>
                                                            <div className="image-upload-container image-upload--large">
                                                                {!this.props.dataSaving && <img src={this.state.images[index].ImageUrl} />}
                                                                <div className="image-upload-overlay"></div>
                                                                <div className={`image-upload-file-container uploader${index}`}>
                                                                    <input id={`fileImg${index}`}
                                                                        type="file"
                                                                        className="custom-file-type"
                                                                        onChange={this.changeSelectedPhoto}
                                                                    />
                                                                    <label htmlFor={`fileImg${index}`}><i className="fa fa-upload"></i></label>
                                                                </div>
                                                                {index !== 0 && <div className="image-delete-file-container">
                                                                    <input id={`fileRemove${index}`}
                                                                        type="button"
                                                                        className="custom-file-type"
                                                                        onClick={this.handleRemoveImage}
                                                                    />
                                                                    <label htmlFor={`fileRemove${index}`}><i className="fa fa-trash-o"></i></label>
                                                                </div>}
                                                            </div>

                                                        </Form.Group>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                            {(this.state.images.length < 5 && this.props.imagesSent === true) && <Button variant="primary" onClick={this.handleAddImage} type="submit">Add Background Image</Button>}

                        </Col>
                    </Row>
                </Container>
                </PerfectScrollbar>
            </div>
        );
    }
}

export default AddBackgroundImage;