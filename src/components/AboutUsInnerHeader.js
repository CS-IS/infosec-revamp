import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import { isAdmin } from '../sp-data/spPermissions';

import '../styles/_innerheader.scss';
import '../styles/_global.scss';

class InnerHeader extends React.Component {

  state = {
    isAdmin : false
  }

  componentDidMount () {
    isAdmin().then((result) => {
        this.setState(()=>({
            isAdmin : result
        }));
    });
  }

  handleModeToggle = () => {
    this.props.handleModeToggle(!this.props.edit);
  }

  handleSave = () => {
    this.props.handleSave("Saved!");
  }

  render() {
    return (
      <div className="inner-header-container">
        <Container className={"inner-header"}>
          <Row style={{height:'33px'}}>
            <Col lg={6}>
              {this.props.text ? <h2>{decodeURIComponent(this.props.text)}</h2> : <h2>Default Text</h2>}
            </Col>
            <Col lg={6}>
              <div className="inner-header-actions">
                {
                  !this.props.edit ? this.state.isAdmin &&
                    <button className="btn-infosec btn-infosec--edit" onClick={this.handleModeToggle}>Edit</button> :
                    <React.Fragment>
                      <button className="btn-infosec btn-infosec--save" onClick={this.handleSave}>Save</button>
                      <button className="btn-infosec btn-infosec--delete" onClick={this.handleModeToggle}>Discard</button>
                    </React.Fragment>
                }
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default InnerHeader;