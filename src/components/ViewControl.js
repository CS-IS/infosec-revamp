/* eslint-disable no-unused-expressions */
import React from 'react';

import {Dropdown,FormControl,Form,FormGroup} from 'react-bootstrap';

import { withRouter } from "react-router";

import '../styles/_sidebar.scss';

class ViewControl extends React.Component {
    state = {
        items: [],
        loadMoreButton: false,
        isLoadingData: false,
        year: [],
        currentPath: this.props.path,
        yearFilter: "",
        titleFilter: "",
        list: this.props.list
    }
    static defaultProps = {
        isDisabled: false
    }      

    createSelectItems() {
        const {year} = this.state;
        let items = [];    
        items.push( <option key={0}>{"-Select Year-"}</option>);     
        for (let i = 0; i < year.length; i++) {             
             items.push( <option key={i+1}>{year[i]}</option>);
        }
        return items;
    }
    
    
    handleTitleInput = key => e => {
        this.setState({ [key]: e.currentTarget.value });
    };
    handleFormSubmit = e => {
        e.preventDefault();
        this.props.onSubmit(this.state)
    };

    handleYearInput = key => e => {
        const re = /^[0-9\b]+$/;
        if (e.target.value === '' || re.test(e.target.value)) {
            this.setState({ [key]: e.target.value });
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps.category != this.props.category){
            this.setState(() => ({
                yearFilter: "",
                titleFilter: ""
            }));
        }
    }
    
    
    
    render() {
        return (
            <div className="sidebar sidebar--disable" >
                <div className="sidebar--disable-search">
                    <Form.Label>Search Items:</Form.Label>
                    <Form onSubmit={this.handleFormSubmit} inline>
                        <FormControl type="text" onChange={this.handleTitleInput("titleFilter")} placeholder="Search" className="mr-sm-2" value={this.state.titleFilter} disabled={this.props.isDisabled} />
                    </Form>
                </div>
                <div className="sidebar--disable-dropdown">
                <Form.Label>Select Year:</Form.Label>
                    <Form onSubmit={this.handleFormSubmit} inline>
                        <FormControl type="text" maxLength="4" value={this.state.yearFilter} onChange={this.handleYearInput("yearFilter")} placeholder="Year" className="mr-sm-2" disabled={this.props.list === "Undertaking" ? true : this.props.isDisabled} />
                    </Form>
                {/* <Form.Label>Select a Year</Form.Label>
                    <Form.Group controlId="exampleForm.ControlSelect1">
                    
                    <Form.Control as="select" onChange={this.handleChange("yearFilter")}>
                        {this.createSelectItems()}
                    </Form.Control>
                </Form.Group> */}
                </div>
                <div className="btn-center">
                    <button className="btn-infosec btn-infosec--add" onClick={(e)=>{
                       this.props.onSubmit(this.state);
                    }}
                    hidden={this.props.isDisabled}
                    >Search</button>
                </div>
            </div>
        );
    }
}

export default withRouter(ViewControl);