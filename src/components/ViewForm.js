import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import InnerHeader from './InnerHeader';
import Row from 'react-bootstrap/Row';
import ViewControl from './ViewControl';
import ViewItem from './ViewItem';

import { withRouter } from "react-router";

import '../styles/_aboutpage.scss';
import '../styles/_global.scss';
import '../styles/_viewform.scss';

class ViewForm extends React.Component {
    render() {

        let innerHeadertext = "";
        if (this.props.match.params.category) {
            if (this.props.match.params.list === this.props.match.params.category) {
                innerHeadertext = `${this.props.match.params.list} > View Item`;
            } else {
                innerHeadertext = `${this.props.match.params.list} > ${this.props.match.params.category} > View Item`;
            }
        } else {
            innerHeadertext = `${this.props.match.params.list} > View Item`;
        }

        return (
            <div className="about-page">
                <div className="about-page-container">
                    <Container fluid="true" className={"no-padding"}>
                        <Row noGutters="true">
                            <Col lg={2}>
                                <ViewControl isDisabled={true}  />
                            </Col>
                            <Col lg={10}>
                                <InnerHeader text={innerHeadertext} />
                                <ViewItem id={this.props.match.params.id} list={this.props.match.params.list} />
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}

export default withRouter(ViewForm);
