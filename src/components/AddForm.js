import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import InnerHeader from './InnerHeader';
import ListForm from './ListForm';
import Row from 'react-bootstrap/Row';
import ViewControl from './ViewControl';

import { addListItems, updateListItemsThumbnail, addFile, createFolder } from '../sp-data/lists/list-item';

import '../styles/_aboutpage.scss';
import '../styles/_global.scss';

export default class AddForm extends React.Component {
    handleAddClick = (list, category, listdata) => {
        addListItems(list, category, listdata).then((result) => {
            const { ID } = result.data;
            createFolder(list, ID).then((result) => {
                if (listdata.listAttachments.thumbnailAttachment != null && listdata.listAttachments.fileAttachment == null) {
                    addFile(list, listdata.listAttachments.thumbnailAttachment, ID, 'Thumbnail').then((result) => {
                        updateListItemsThumbnail(list, ID, listdata.listAttachments.thumbnailAttachment.name).then(() => {
                            this.props.history.goBack();
                        });
                        
                    });
                } else if (listdata.listAttachments.thumbnailAttachment != null && listdata.listAttachments.fileAttachment != null) {
                    addFile(list, listdata.listAttachments.thumbnailAttachment, ID, 'Thumbnail').then((result) => {
                        updateListItemsThumbnail(list, ID, listdata.listAttachments.thumbnailAttachment.name).then(() => {
                            addFile(list, listdata.listAttachments.fileAttachment, ID, 'Attachment').then((result) => {
                                this.props.history.goBack();
                            });
                        });                        
                    });                    
                } else if (listdata.selectedType === "Image") {
                    addFile(list, listdata.listAttachments.fileAttachment, ID, 'Thumbnail').then((result) => {
                        updateListItemsThumbnail(list, ID, listdata.listAttachments.fileAttachment.name).then(() => {
                            this.props.history.goBack();
                        });
                    });
                }
            })
        });
    }
    render() {
        console.log(this.props);;
        let innerHeadertext = "";
        if (this.props.match.params.category) {
            if (this.props.match.params.list === this.props.match.params.category) {
                innerHeadertext = `${this.props.match.params.list} > Add New`;
            } else {
                innerHeadertext = `${this.props.match.params.list} > ${this.props.match.params.category} > Add New`;
            }
        } else {
            innerHeadertext = `${this.props.match.params.list} > Add New`;
        }
        return (
            <div className="about-page">
                <div className="about-page-container">
                    <Container fluid="true" className={"no-padding"}>
                        <Row noGutters="true">
                            <Col lg={2}>
                                <ViewControl isDisabled={true} />
                            </Col>
                            <Col lg={10}>
                                <InnerHeader text={innerHeadertext} />
                                <ListForm
                                    list={decodeURIComponent(this.props.match.params.list).replace("'", "")}
                                    category={this.props.match.params.category}
                                    handleAddClick={this.handleAddClick}
                                />
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}