import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Swiper from 'react-id-swiper';
import Slider from "react-slick";

import { config } from '../environment/environment';
import { getFilesForSlider } from '../sp-data/lists/list-content';
import { Link } from 'react-router-dom';

import '../styles/_homepage.scss';

import "slick-carousel/slick/slick.css";
/* import "slick-carousel/slick/slick-theme.css"; */

const PreviousButton = (props) => {
  const { className, style, onClick } = props;
  return (
    <button onClick={onClick} style={{ zIndex: 5 }} type='button' className={`slick-btn slick-prev pull-left`}><i className='fa fa-angle-left' aria-hidden='true' style={{ marginLeft: "-2px", marginTop: '-2px' }}></i></button>
  );
}

const NextButton = (props) => {
  const { className, style, onClick } = props;
  return (
    <button onClick={onClick} type='button' className={`slick-btn slick-next pull-right`} ><i style={{ marginLeft: "3px", marginTop: '-2px' }} className='fa fa-angle-right' aria-hidden='true'></i></button>
  );
}


class WhatsNewSlider extends React.Component {
  state = {
    carouselPictures: [],
    retrievalStatus: false
  }

  componentDidMount() {
    const carouselPictures = [];
    getFilesForSlider().then(result => {
      result.forEach(item => {
        carouselPictures.push(
          {
            imageUrl: config.meralcoUrl + "/" + item.url,
            id: item.id
          }
        );
      });
      this.setState((prevState) => {
        return {
          carouselPictures,
          retrievalStatus: true
        }
      });
    })

  }

  render() {
    return (
      <React.Fragment>
        {
          this.state.retrievalStatus ?
            <Slider
              prevArrow={<PreviousButton />}
              nextArrow={<NextButton />}
              className="custom-slick"
              accessibility={false}
              centerMode={true}
              variableWidth={true}
              slidesToShow={1}
              infinite={false}
            /* prevArrow="<button type='button' class='slick-btn slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>"
            nextArrow="<button type='button' class='slick-btn slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>" */

            >
              <div className="custom-slick__image-container"><img src={require("../assets/images/landscape.png")} alt="" />
              </div>
              <div className="custom-slick__image-container"><img src={require("../assets/images/sweet.PNG")} alt="" /></div>
              <div className="custom-slick__image-container"><img src={require("../assets/images/landscape.png")} alt="" /></div>

              {
                this.state.carouselPictures.length !== 0 ? this.state.carouselPictures.map((item, index) => {
                  return (
                    <div
                      key={index}
                      className="custom-slick__image-container"
                    >
                      <Link to={`/Whats New/ViewItem/${item.id}`}><img src={`${item.imageUrl}`} height="100%" width="100%" /></Link>
                    </div>
                  )
                }) :
                  <div>Nothing!</div>
              }

            </Slider>
            :
            <div>Loading</div>
        }
      </React.Fragment>

    )
  }
}

export default WhatsNewSlider;

{/* <Swiper
            containerClass={"swiperContainerPerspective"}
            effect ={'fade'}
            
            grabCursor = {'true'}
            centeredSlides = {true}
            loop = {true}
            lazy = {true}
            slidesPerView = {0}
            pagination = {
                {
                el: '.swiper-pagination',
                clickable: true
                }
            }
            navigation = {
                {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
                }
            }
            renderPrevButton = {() => <button className="swiper-button-prev"><i className="fa fa-chevron-left" aria-hidden="true"></i></button>}
            renderNextButton = {() => <button className="swiper-button-next"><i className="fa fa-chevron-right" aria-hidden="true"></i></button>}
            spaceBetween = {-250}
          >
            {
             this.state.carouselPictures.length !== 0 ? this.state.carouselPictures.map((item, index) => {
                return (
                  <div 
                  key={index} 
                  className='slide'
                  >
                    <Link to={`/Whats New/ViewItem/${item.id}`}><img src={`${item.imageUrl}`} height="100%" width="375px"/></Link>
                  </div>
                )
              }) :
              <div>Nothing!</div>
            }          
            </Swiper> */}   