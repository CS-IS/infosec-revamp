import React from 'react';

import "react-datepicker/dist/react-datepicker.css";
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import DatePicker from "react-datepicker";
import { Editor } from '@tinymce/tinymce-react';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';

import { addItemToList, getItemById } from "../sp-data/lists/list-content";
import { withRouter } from "react-router";

import '../styles/_forms.scss';
import '../styles/_global.scss';

class WhatsNewForm extends React.Component {
    fileType = ["Text", "PDF", "Image", "PowerPoint", "Video"];

    state = {
        selectedType: "Text",
        details: false,
        attachment: false,
        thumbnail: false,
        video: false,
        startDate: new Date(),
        module: this.props.module || "",
        category: this.props.category || ""
    }

    changeFileType(filetype) {
        switch (filetype) {
            case "Text":
                this.setState({
                    selectedType: filetype,
                    details: true,
                    attachment: false,
                    thumbnail: true,
                    video: false
                });
                break;
            case "PDF":
                console.log("PDF");
                this.setState({
                    selectedType: filetype,
                    details: false,
                    attachment: true,
                    thumbnail: true,
                    video: false
                });
                break;
            case "PowerPoint":
                console.log("PPT");
                this.setState({
                    selectedType: filetype,
                    details: false,
                    attachment: true,
                    thumbnail: true,
                    video: false
                });
                break;
            case "Image":
                console.log("Img");
                this.setState({
                    selectedType: filetype,
                    details: true,
                    attachment: true,
                    thumbnail: false,
                    video: false
                });
                break;
            case "Video":
                console.log("Vid");
                this.setState({
                    selectedType: filetype,
                    details: false,
                    attachment: false,
                    thumbnail: true,
                    video: true
                });
                break;
            default:
                this.setState({
                    ...this.state
                });
                break;
        }
    }

    handleDropdownChange = (e) => {
        this.changeFileType(e.target.value);
    }

    handleChange = (date) => {
        this.setState({
            startDate: date
        });
    }

    handleDiscardClick = () => {
        this.props.history.goBack();
    }

    handleTitleChange = e => {
        var title = e.currentTarget.value;
        this.setState({
            title
        });
        
    }

    handleDetailsChange = e => {
        var details = e.target.getContent();
        this.setState({
            details
        });
    } 

    addToSharePoint = () => {
        let data = {};

        switch (this.state.selectedType) {
            case "Text":
                data = {
                    OData__Type: this.state.selectedType,
                    Title: this.state.title,
                    DateReleased: this.state.startDate,
                    Details: this.state.details,
                    // Thumbnail: this.state.
                }
                break;
            case "PDF":
                data = {
                    OData__Type: this.state.selectedType,
                    Title: this.state.title,
                    DateReleased: this.state.startDate,
                    // file: this.state.file
                    // Thumbnail: this.state.
                }
                break;
            case "PowerPoint":
                console.log("PPT");
                data = {
                    OData__Type: this.state.selectedType,
                    Title: this.state.title,
                    DateReleased: this.state.startDate,
                    // file: this.state.file
                    // Thumbnail: this.state.
                }
                break;
            case "Image":
                console.log("Img");
                data = {
                    OData__Type: this.state.selectedType,
                    Title: this.state.title,
                    DateReleased: this.state.startDate,
                    Details: this.state.details,
                    // Thumbnail: this.state.
                }
                break;
            case "Video":
                console.log("Vid");
                data = {
                    OData__Type: this.state.selectedType,
                    Title: this.state.title,
                    DateReleased: this.state.startDate,
                    // Thumbnail: this.state.
                    // video: this.state
                }
                break;
            default:
                this.setState({
                    ...this.state
                });
                break;
        }

        addItemToList("Whats New", data).then((item)=>{
            console.log(item);
        });
    }

    componentDidMount() {
        if (this.state.category === "Awareness") {
            this.changeFileType("Image");
        } else {
            this.changeFileType(this.state.selectedType);
        }

        //If mode is Edit, load item metadata
        const { id } = this.props.match.params;
        if (this.props.mode !== "Add") {
            getItemById(id).then((data) => {
                console.log(data);

                this.setState(() => ({
                    title: data.Title,
                    dateReleased: new Date(data.DateReleased)
                }));
            });
        }
    }

    getFile = (e) => {
        let file = e.target.files[0];
        let reader = new FileReader();
        reader.readAsDataURL(file);

        this.setState({
            file: e.target.files[0]
        })
        const mbSize = e.target.files[0].size / 1024 / 1024;
        if (e.target.id == "fileThumbnail") {
            if (mbSize > 1) {
                alert("Maximum attachment size is up to 1MB only");
                e.target.value = null;
            }
        }
        else {
            if (mbSize > 20) {
                alert("Maximum attachment size is up to 20MB only");
                e.target.value = null;
            }
        }
    }

    render() {
        const { module, category, thumbnail } = this.state;
        return (
            <Container className={"form-background with-padding"}>
                <Row>
                    <Col md={4}>
                        <Form.Group>
                            <Form.Label>Title *</Form.Label>
                            <Form.Control id="txtTitle"
                                placeholder="Title"
                                maxLength="255"
                                value={this.state.title || ""}
                                onChange={this.handleTitleChange}
                                />
                        </Form.Group>
                    </Col>

                    <Col md={4}>
                        <Form.Group>
                            <Form.Label>Date Released</Form.Label>
                            <br />
                            <DatePicker id="txtDateReleased"
                                selected={this.state.startDate}
                                onChange={this.handleChange}
                                className="form-control"
                                value={this.state.startDate} />
                        </Form.Group>
                    </Col>
                </Row>

                <Row>
                    <Col md={4}>
                        <Form.Group>
                            <Form.Label>Select a type *</Form.Label>
                            <Form.Control id="ddnFileType"
                                as="select"
                                onChange={this.handleDropdownChange}
                                value={this.state.selectedType}
                                validationOption={{
                                    name: 'Type', // Optional.[String].Default: "". To display in the Error message. i.e Please enter your {name}.
                                    check: true, // Optional.[Bool].Default: true. To determin if you need to validate.
                                    required: true // Optional.[Bool].Default: true. To determin if it is a required field.
                                  }}>
                                {
                                    (() => {
                                        switch (module) {
                                            case "Policies":
                                                this.fileType = this.fileType.filter(i => i !== "Image").filter(v => v !== "Video");
                                                return (this.fileType.map((file) => {
                                                    return <option
                                                        value={file}
                                                        key={file}>{file}</option>
                                                }));
                                            default:
                                                if (category === "Awareness") {
                                                    return <option
                                                        value="Image"
                                                        key="Image">Image</option>
                                                } else {
                                                    return (this.fileType.map((file) => {
                                                        return <option
                                                            value={file}
                                                            key={file}>{file}</option>
                                                    }));
                                                }
                                        }
                                    })()
                                }
                            </Form.Control>
                        </Form.Group>
                    </Col>
                </Row>

                <Row style={{ display: this.state.details ? 'block' : 'none' }}>
                    <br />
                    <Col md={9}>
                        <Form.Group>
                            <Editor
                                apiKey="API_KEY"
                                init={{
                                    plugins: 'link table',
                                    height: 270
                                }}
                                onChange={this.handleDetailsChange}
                            />
                        </Form.Group>
                    </Col>
                </Row>

                <Row style={{ display: this.state.attachment ? 'block' : 'none' }}>
                    <Col md={4}>
                        <Form.Group>
                            <Form.Label>{this.state.selectedType === "Image" ? "Upload Photo" : "File Attachment"}</Form.Label>
                            <input id="fileAttachment"
                                type="file"
                                className="form-control-file"
                                onChange={this.getFile} />
                            <small className="form-text text-muted">Note: File must be 20MB or below</small>
                        </Form.Group>
                    </Col>
                </Row>

                <Row style={{ display: this.state.video ? 'block' : 'none' }}>
                    <Col md={4}>
                        <Form.Group>
                            <Form.Label>Video Link</Form.Label>
                            <input id="txtVideoLink"
                                type="text"
                                className="form-control"
                                placeholder="Video Link"
                                pattern="^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$" />
                        </Form.Group>
                    </Col>
                </Row>

                <Row style={{ display: this.state.thumbnail ? 'block' : 'none' }}>
                    <Col md={4}>
                        <Form.Group>
                            <Form.Label>Thumbnail</Form.Label>
                            <input id="fileThumbnail"
                                type="file"
                                className="form-control-file"
                                validationOption={{
                                    name: 'Name', // Optional.[String].Default: "". To display in the Error message. i.e Please enter your {name}.
                                    check: true, // Optional.[Bool].Default: true. To determin if you need to validate.
                                    required: {thumbnail} // Optional.[Bool].Default: true. To determin if it is a required field.
                                  }}
                                onChange={this.getFile} />
                            <small id="emailHelp" className="form-text text-muted">Note: Image must be 1MB or below</small>
                        </Form.Group>
                    </Col>
                </Row>

                <ButtonToolbar>
                    <Button variant="primary" type="submit" onClick={this.addToSharePoint}>Add</Button>
                    <Button variant="light" onClick={this.handleDiscardClick} className="btn-infosec btn-infosec--delete">Discard</Button>
                </ButtonToolbar>
            </Container>
        );
    }
}

export default withRouter(WhatsNewForm);