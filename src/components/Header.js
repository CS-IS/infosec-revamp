import React from 'react';

import { Link } from 'react-router-dom';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';

import { isAdmin } from '../sp-data/spPermissions';
import { LinkContainer } from 'react-router-bootstrap';
import { links } from '../sp-data/sp-links';

import '../styles/_main-navigation.scss';

class Header extends React.Component {
    state = {
        isAdmin: false
    }

    componentDidMount () {
        isAdmin().then((result) => {
            this.setState(()=>({
                isAdmin : result
            }));
        });
    }

    render() {
        const navSecurityEducation = (<p>Security Education<i className="ms-Icon ms-Icon--ChevronDown"></i></p>)
        const navPolicies = (<p>Policies<i className="ms-Icon ms-Icon--ChevronDown"></i></p>)
        const navSettings = (<p>Admin<i className="ms-Icon ms-Icon--ChevronDown"></i></p>)
        console.log(this.state);

        return(
            <div className="main-navigation">
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <Link className="navbar-brand" to='/'>
                        <img src={require("../assets/images/logo1.png")} width="175" alt="Infosec-logo"/>
                    </Link>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav>
                            <LinkContainer to='/'>
                                <Nav.Link>Home</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to='/AboutUs'>
                                <Nav.Link>About Us</Nav.Link>
                            </LinkContainer>
                            <NavDropdown title={navSecurityEducation} id="collasible-nav-dropdown">
                                <LinkContainer to='/Security Education/Awareness'>
                                    <NavDropdown.Item>Awareness</NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to='/Security Education/Training & Education'>
                                    <NavDropdown.Item>Training & Education</NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to='/Security Education/Cybersecurity Day'>
                                    <NavDropdown.Item>Cybersecurity Day</NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to='/Security Education/Others'>
                                    <NavDropdown.Item>Others</NavDropdown.Item>
                                </LinkContainer>
                            </NavDropdown>
                            <NavDropdown title={navPolicies} id="collasible-nav-dropdown">
                                <LinkContainer to='/Policies/Policies'>
                                    <NavDropdown.Item>Policies</NavDropdown.Item>
                                </LinkContainer>

                                <LinkContainer to='/Policies/Guidelines'>
                                    <NavDropdown.Item>Guidelines</NavDropdown.Item>
                                </LinkContainer>

                                <LinkContainer to='/Policies/Standards'>
                                    <NavDropdown.Item>Standards</NavDropdown.Item>
                                </LinkContainer>

                                <LinkContainer to='/Policies/Procedures'>
                                    <NavDropdown.Item>Procedures</NavDropdown.Item>
                                </LinkContainer>

                                <LinkContainer to='/Undertaking'>
                                    <NavDropdown.Item>InfoSec Forms</NavDropdown.Item>
                                </LinkContainer>
                            </NavDropdown>
                            <LinkContainer to='/Insights'>
                                <Nav.Link>Insights</Nav.Link>
                            </LinkContainer>
                            <NavDropdown title={navSettings} id="collasible-nav-dropdown" style={{ display: this.state.isAdmin ? 'block' : 'none' }}>
                                <LinkContainer to='/Settings/Admin/AddBackgroundImage'>
                                    <NavDropdown.Item>Background Images</NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to='/WhatsNew'>
                                    <NavDropdown.Item>What's New?</NavDropdown.Item>
                                </LinkContainer>                            
                                <NavDropdown.Item href={links.header.siteUsage}> Site Usage </NavDropdown.Item>
                                <NavDropdown.Item href={links.header.oldSite} target="_blank"> Link to Old Site </NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
}

export default Header;