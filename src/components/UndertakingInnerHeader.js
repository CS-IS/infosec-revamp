import React from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { isAdmin } from '../sp-data/spPermissions';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { deleteListItem } from "../sp-data/lists/list-item";

import '../styles/_innerheader.scss';
import '../styles/_global.scss';
import '../styles/_buttons.scss';

class InnerHeader extends React.Component {

  state = {
    isAdmin: false
  }

  componentDidMount() {
    isAdmin().then((result) => {
      this.setState(() => ({
        isAdmin: result
      }));
    });
  }

  handleSave = () => {
    this.props.handleSave("Saved!");
  }

  deleteListItem = () => {
    const id = this.props.id;
    Swal.fire({
        title: "Are you sure you want to delete this item?",
        showCancelButton: true,
        confirmButtonColor: '#3cb371',
        cancelButtonColor: '#dc143c',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    }).then((result) => {
        if (result.value) {
            this.setState({
                dataSaving: true
            });
            deleteListItem("Undertaking", id).then(() => {
                this.props.history.push("/Undertaking");
            });
        }
    });       
  }

  render() {
    console.log(this.props)
    return (
      <div className="inner-header-container">
        <Container className={"inner-header"}>
          <Row style={{ height: '33px' }}>
            <Col lg={6}>
              {this.props.text ? <h2>{decodeURIComponent(this.props.text)}</h2> : <h2>Default Text</h2>}
            </Col>
            <Col lg={6}>
              <div className="inner-header-actions">
                {
                  this.state.isAdmin &&
                    <Link to={"../EditItem/" + this.props.id}
                      className="btn-infosec btn-infosec--edit"
                      style={{ textAlign: "center" }}>
                      Edit
                    </Link>
                }
                {
                  this.state.isAdmin &&
                  <button style={{ marginLeft: '15px' }}
                  onClick={this.deleteListItem}
                    className={`btn-infosec btn-infosec--delete ${this.state.itemCount >= 5 ? "btn-infosec--disabled" : null}`}
                  >
                    Delete
                  </button>
                }
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default InnerHeader;