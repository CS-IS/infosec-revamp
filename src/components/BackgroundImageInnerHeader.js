import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Swal from 'sweetalert2';

import '../styles/_innerheader.scss';
import '../styles/_global.scss';
import '../styles/_buttons.scss';

class InnerHeader extends React.Component {

  handleModeToggle = () => {
    this.props.handleModeToggle(!this.props.edit);
  }

  handleSave = () => {
    Swal.fire({
      title: 'Are you sure you want to save changes?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3cb371',
      cancelButtonColor: '#dc143c',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.props.handleSave();
      }
    });
  }

  handleDiscard = () => {
    Swal.fire({
      title: 'Return background images to last save?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3cb371',
      cancelButtonColor: '#dc143c',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      console.log(result);
      if (result.value) {
        this.props.handleDiscard()
      }
    });
  }

  render() {
    return (
      <div className="inner-header-container">
        <Container className={"inner-header"}>
          <Row style={{height:'33px'}}>
            <Col lg={6}>
              {this.props.text ? <h2>{decodeURIComponent(this.props.text)}</h2> : <h2>Default Text</h2>}
            </Col>
            <Col lg={6}>
              <div className="inner-header-actions">
                {
                  <React.Fragment>
                    <button className="btn-infosec btn-infosec--save" onClick={this.handleSave}>Save</button>
                    <button className="btn-infosec btn-infosec--delete" onClick={this.handleDiscard}>Discard</button>
                  </React.Fragment>
                }
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default InnerHeader;