import React from 'react';

import { Editor } from '@tinymce/tinymce-react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';
import Loader from './Loader';

import { addItemToList, getItemById, checkIfUndertakingExisting } from "../sp-data/lists/list-content";
import { updateUndertaking, deleteListItem } from "../sp-data/lists/list-item";
import { withRouter } from "react-router";

import '../styles/_forms.scss';
import '../styles/_global.scss';
import '../styles/_aboutpage.scss';
import '../styles/_buttons.scss';

class UndertakingForm extends React.Component {
    isInEdit = this.props.location.pathname.includes("EditItem");
    errorMessage = "This field is required.";

    state = {
        title: "",
        prevTitle: "",
        details: "",
        titleError: "",
        detailsError: "",
        dataSaving: false,
        dataRetrieval : false
    }

    handleDiscardClick = () => {
        this.props.history.goBack();
    }

    handleTitleChange = e => {
        var title = e.currentTarget.value;
        this.setState({
            title
        });
    }

    handleDetailsChange = value => {
        this.setState({
            details : value
        });
    }

    addToSharePoint = () => {
        const { id } = this.props;
        const invalidElements = this.validateForm();

        if (invalidElements.length > 0) {
            invalidElements.map((element) => {
                let states = element + "Error";
                this.setState({
                    [states]: this.errorMessage
                });
            });
        }
        else {
            checkIfUndertakingExisting("Undertaking", this.state.title).then((result)=>{
                if (!this.isInEdit && result.length > 0) {
                    this.setState({
                        titleError: "Title already exists."
                    });
                }
                else {
                    let data = {};
                    data = {
                        Title: this.state.title,
                        Details: this.state.details
                    }

                    this.updateItems(data, id);
                }
            });
        }
    }


    updateItems = async (data, id) => {
        const result = await Swal.fire({
            title: (this.isInEdit ? "Are you sure you want to save changes?" : 'Are you sure you want to add this item?'),
            showCancelButton: true,
            confirmButtonColor: '#3cb371',
            cancelButtonColor: '#dc143c',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        });
        if (result.value) {
            this.setState({
                dataSaving: true
            });

            if (!this.isInEdit) {
                addItemToList("Undertaking", data).then((item) => {
                    this.props.history.goBack();
                });
            } else {
                await updateUndertaking("Undertaking", id, data);
                const items = await checkIfUndertakingExisting("Acknowledgements", id);
                items.map((item) => {
                    updateUndertaking("Acknowledgements", item.Id, { UndertakingTitle: this.state.title }).then(() => {
                        this.props.history.goBack();
                    });
                });
            }
        }
    }

    validateForm = () => {
        const { titleError,
            detailsError,

            title,
            details,
        } = this.state;

        const elementError = ["title", "details"];
        elementError.map((element) => {
            let item = element + "Error";
            this.setState({
                [item]: ""
            });
        });

        let isValid = false;
        let invalidElements = [];

        if (!title.trim()) { invalidElements.push("title"); }
        if (!details) { invalidElements.push("details"); }

        return invalidElements;
    }

    componentDidMount() {
        //If mode is Edit, load item metadata
        const { id } = this.props.match.params;
        if (this.props.mode !== "Add") {
            getItemById(id, "Undertaking").then((data) => {
                this.setState(() => ({
                    title: data.Title,
                    prevTitle: data.Title,
                    details: data.Details,
                    dataRetrieval : true
                }));
            });
        }else{
            this.setState(() => ({
                dataRetrieval : true
            }));
        }
    }

    deleteListItem = () => {
        console.log(this.props);
        Swal.fire({
            title: "Are you sure you want to delete this item?",
            showCancelButton: true,
            confirmButtonColor: '#3cb371',
            cancelButtonColor: '#dc143c',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                this.setState({
                    dataSaving: true
                });
                deleteListItem("Undertaking", this.props.id).then(() => {
                    this.props.history.push("/Undertaking");
                });
            }
        });       
    }

    render() {
        return (
            <Container className={"form-background with-padding"}>
                {
                    this.state.dataSaving &&
                    <div style={{ height: '75%', width: '100%', position: 'absolute' }}>
                        <Loader />
                    </div>
                }

                <Row>
                    <Col md={4}>
                        <Form.Group>
                            <Form.Label>Title *</Form.Label>
                            <Form.Control id="txtTitle"
                                placeholder="Title"
                                maxLength="255"
                                value={this.state.title || ""}
                                onChange={this.handleTitleChange}
                            />
                            <div className='invalid-feedback'
                                style={{ display: this.state.titleError ? 'block' : 'none' }}>
                                {this.state.titleError}
                            </div>
                        </Form.Group>
                    </Col>
                </Row>

                <Row>
                    <Col md={9}>
                        <Form.Group>
                            <Form.Label>Details *</Form.Label>
                            {this.state.dataRetrieval && <Editor
                                apiKey="API_KEY"
                                init={{
                                    plugins: 'link table',
                                    height: 350,
                                    statusbar: false
                                }}
                                onEditorChange={this.handleDetailsChange}
                                initialValue={this.state.details}
                            />}
                            <div className='invalid-feedback'
                                style={{ display: this.state.detailsError ? 'block' : 'none' }}>
                                {this.state.detailsError}
                            </div>
                        </Form.Group>
                    </Col>
                </Row>
                <br />
                <ButtonToolbar>
                <Button className='btn-infosec btn-infosec--save' type="submit" onClick={this.addToSharePoint}>Save</Button>
                    <button onClick={this.handleDiscardClick} type="button" class="btn-infosec btn-infosec--delete">Discard</button>
                </ButtonToolbar>
            </Container>
        );
    }
}

export default withRouter(UndertakingForm);