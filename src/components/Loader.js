import React from 'react';

import Spinner from 'react-bootstrap/Spinner';

const AppLoader = () => {
  return (
    <div className='loader-wrapper'>
      {/* <Loader
      className="loader"
        type={"RevolvingDot"}
        color={"#60C0FF"}
        height={100}
        width={100}
      /> */}
      <Spinner 
      size={'md'}
      animation="border" 
      variant="primary" />
    </div>
  )
}

export default AppLoader;