import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Loader from './Loader';
import Row from 'react-bootstrap/Row';

import '../styles/_aboutpage.scss';
import '../styles/_global.scss';

const BigImage = (props) => {
    return (
        <Col lg={12}>
            <div className="about-image about-image--large">
                <div className="about-image-container">
                    <img alt='Loading' src={props.imageSource} />
                </div>
                <h3>{props.caption}</h3>
            </div>
        </Col>
    )
};

const SmallImage = (props) => {
    return (
        <Col lg={6}>
            <div className="about-image about-image--small">
                <div className="about-image-container">
                    <img alt='Loading' src={props.imageSource} />
                </div>
                <h3>{props.caption}</h3>
            </div>
        </Col>
    )
}

const AboutUsView = (props) => {
    console.log(props);
    return (
        <React.Fragment>
            {props.viewContentStatus ?
                props.itemRetrieved ?
                    ((<Container><Row>
                        <Col lg={5} md={5} className={"no-padding"}>
                            <div className="about-gallery-container">
                                <Container>
                                    <Row>
                                        {
                                            props.Image1 && <BigImage imageSource={props.Image1} caption={props.Caption1} />
                                        }
                                        {
                                            props.Image2 && <SmallImage imageSource={props.Image2} caption={props.Caption2} />
                                        }
                                        {
                                            props.Image3 && <SmallImage imageSource={props.Image3} caption={props.Caption3} />
                                        }
                                        {
                                            props.Image4 && <SmallImage imageSource={props.Image4} caption={props.Caption4} />
                                        }
                                        {
                                            props.Image5 && <SmallImage imageSource={props.Image5} caption={props.Caption5} />
                                        }
                                    </Row>
                                </Container>
                            </div>
                        </Col>
                        <Col lg={7} md={7}>
                            <div
                                className="about-page-content-text"
                                dangerouslySetInnerHTML={{ __html: props.Body }}
                            >
                            </div>
                        </Col>
                    </Row>
                    </Container>))
                    :
                    (<div style={{ color: "white" }}>Start Adding About Us Content!</div>)
                :
                (
                    <Loader/>
                )
            }
        </React.Fragment>

    );
}

export default AboutUsView;