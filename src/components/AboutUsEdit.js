import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import { Editor } from '@tinymce/tinymce-react';
import Form from 'react-bootstrap/Form';
import Loader from './Loader';
import Row from 'react-bootstrap/Row';
import Swal from 'sweetalert2';

import '../styles/_aboutpage.scss';
import '../styles/_forms.scss';
import '../styles/_global.scss';

export default class AboutUs_Edit extends React.Component {
    state = {
        Id: this.props.content.Id || null,
        Body: this.props.content.Body || '',
        Image1: this.props.content.Image1 || undefined,
        Image2: this.props.content.Image2 || undefined,
        Image3: this.props.content.Image3 || undefined,
        Image4: this.props.content.Image4 || undefined,
        Image5: this.props.content.Image5 || undefined,
        File1: undefined,
        File2: undefined,
        File3: undefined,
        File4: undefined,
        File5: undefined,
        Caption1: this.props.content.Caption1 || "",
        Caption2: this.props.content.Caption2 || "",
        Caption3: this.props.content.Caption3 || "",
        Caption4: this.props.content.Caption4 || "",
        Caption5: this.props.content.Caption5 || ""
    }

    componentDidUpdate() {
        console.log(this.props);
        if (this.props.content.dataFlag === true) {
            if (!this.props.itemRecieved) {
                Swal.fire({
                    title: 'Are you sure you want to save changes?',
                    //type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3cb371',
                    cancelButtonColor: '#dc143c',
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No'
                }).then((result) => {
                    console.log(result);
                    if (result.value) {
                        this.props.handleStartSave({
                            ...this.state,
                            itemRetrieved: true
                        });
                    } else {
                        this.props.handleFailedValidation();
                    }
                });
            }
        }
    }

    validate(image, body) {
        let err = false;
        if (!image && !body)
            err = true;
        return err;
    }

    onLabelChange = (e) => {
        const Id = e.target.id;
        const label = "Caption" + Id[Id.length - 1];
        this.setState({
            [label]: e.target.value
        });
    }

    changeSelectedPhoto = (e) => {
        const file = e.target.files[0];
        const mbSize = file.size / 1024 / 1024;
        const fileType = file.type === 'image/jpeg' || file.type === 'image/png';

        if (!fileType) {            
            Swal.fire({
                title: 'Selected file is not valid',
                type: 'error',
                confirmButtonColor: '#3cb371',
                cancelButtonColor: '#dc143c',
                confirmButtonText: 'Ok'
            });
            e.target.value = null;
            return;            
        }

        if (mbSize > 5) {
            Swal.fire({
                title: 'Maximum attachment size is up to 5MB only',
                type: 'error',
                confirmButtonColor: '#3cb371',
                cancelButtonColor: '#dc143c',
                confirmButtonText: 'Ok'
            });
            e.target.value = null;
            return;
        }

        const Id = e.target.id;
        const Image = "Image" + Id[Id.length - 1];
        const File = "File" + Id[Id.length - 1];

        this.setState({
            [Image]: URL.createObjectURL(e.target.files[0]),
            [File]: e.target.files[0]
        });
    }

    handleImageRemove = (e) => {
        const btnId = e.target.id;
        switch (btnId) {
            case "fileRemove1":
                document.getElementById("fileImg1").value = null;
                this.setState(() => ({
                    ...this.state,
                    Image1: undefined
                }));
                break;
            case "fileRemove2":
                document.getElementById("fileImg2").value = null;
                this.setState(() => ({
                    ...this.state,
                    Image2: undefined
                }));
                break;
            case "fileRemove3":
                document.getElementById("fileImg3").value = null;
                this.setState(() => ({
                    ...this.state,
                    Image3: undefined
                }));
                break;
            case "fileRemove4":
                document.getElementById("fileImg4").value = null;
                this.setState(() => ({
                    ...this.state,
                    Image4: undefined
                }));
                break;
            case "fileRemove5":
                document.getElementById("fileImg5").value = null;
                this.setState(() => ({
                    ...this.state,
                    Image5: undefined
                }));
                break;
            default:
                break;
        }
    }

    checkSpecialCharacter(filename) {
        var re = new RegExp("^[^<>%$=!#%@|]*$");
        var specialchars = re.test(filename);
        return specialchars;
    }

    handleEditorChange = (e) => {
        this.setState(() => ({
            Body: e.target.getContent()
        }));
    }

    render() {
        return (
            <React.Fragment>
                {this.props.content.dataFlag && <Loader />}
                <Container>
                    <Row>
                        <Col lg={5}>
                            <Row>
                                <Col lg={12}>
                                    <Form.Group>
                                        <div className="image-upload-container image-upload--large">
                                            <img alt='Loading' src={this.state.Image1 || require("../assets/images/defaultuploaderimg.png")} />
                                            <div className="image-upload-overlay"></div>
                                            <div className="image-upload-file-container">
                                                <input id="fileImg1"
                                                    type="file"
                                                    className="custom-file-type"
                                                    onChange={this.changeSelectedPhoto}
                                                />
                                                <label htmlFor="fileImg1"><i className="fa fa-upload"></i></label>
                                            </div>
                                            <div className="image-delete-file-container">
                                                <input id="fileRemove1"
                                                    type="button"
                                                    className="custom-file-type"
                                                    onClick={this.handleImageRemove}
                                                />
                                                <label htmlFor="fileRemove1"><i className="fa fa-trash-o"></i></label>
                                            </div>
                                        </div>
                                        <Form.Control id="txtLabel1"
                                            value={this.state.Caption1}
                                            className="text-center"
                                            onChange={this.onLabelChange}
                                            placeholder="Image Caption"
                                            required />
                                    </Form.Group>
                                </Col>
                            </Row>

                            <Row>
                                <Col lg={6}>
                                    <Form.Group>
                                        <div className="image-upload-container image-upload--small">
                                            <img alt='Loading' src={this.state.Image2 || require("../assets/images/defaultuploaderimg.png")} />
                                            <div className="image-upload-overlay"></div>
                                            <div className="image-upload-file-container">
                                                <input id="fileImg2"
                                                    type="file"
                                                    className="custom-file-type"
                                                    onChange={this.changeSelectedPhoto}
                                                />
                                                <label htmlFor="fileImg2"><i className="fa fa-upload"></i></label>
                                            </div>
                                            <div className="image-delete-file-container">
                                                <input id="fileRemove2"
                                                    type="button"
                                                    className="custom-file-type"
                                                    onClick={this.handleImageRemove}
                                                />
                                                <label htmlFor="fileRemove2"><i className="fa fa-trash-o"></i></label>
                                            </div>
                                        </div>
                                        <Form.Control id="txtLabel2"
                                            value={this.state.Caption2}
                                            className="text-center"
                                            onChange={this.onLabelChange}
                                            placeholder="Image Caption"
                                            required />
                                    </Form.Group>
                                </Col>

                                <Col lg={6}>
                                    <Form.Group>
                                        <div className="image-upload-container image-upload--small">
                                            <img alt='Loading' src={this.state.Image3 || require("../assets/images/defaultuploaderimg.png")} />
                                            <div className="image-upload-overlay"></div>
                                            <div className="image-upload-file-container">
                                                <input id="fileImg3"
                                                    type="file"
                                                    className="custom-file-type"
                                                    onChange={this.changeSelectedPhoto}
                                                />
                                                <label htmlFor="fileImg3"><i className="fa fa-upload"></i></label>
                                            </div>
                                            <div className="image-delete-file-container">
                                                <input id="fileRemove3"
                                                    type="button"
                                                    className="custom-file-type"
                                                    onClick={this.handleImageRemove}
                                                />
                                                <label htmlFor="fileRemove3"><i className="fa fa-trash-o"></i></label>
                                            </div>
                                        </div>
                                        <Form.Control id="txtLabel3"
                                            value={this.state.Caption3}
                                            className="text-center"
                                            onChange={this.onLabelChange}
                                            placeholder="Image Caption"
                                            required />
                                    </Form.Group>
                                </Col>
                            </Row>

                            <Row>
                                <Col lg={6}>
                                    <Form.Group>
                                        <div className="image-upload-container image-upload--small">
                                            <img alt='Loading' src={this.state.Image4 || require("../assets/images/defaultuploaderimg.png")} />
                                            <div className="image-upload-overlay"></div>
                                            <div className="image-upload-file-container">
                                                <input id="fileImg4"
                                                    type="file"
                                                    className="custom-file-type"
                                                    onChange={this.changeSelectedPhoto}
                                                />
                                                <label htmlFor="fileImg4"><i className="fa fa-upload"></i></label>
                                            </div>
                                            <div className="image-delete-file-container">
                                                <input id="fileRemove4"
                                                    type="button"
                                                    className="custom-file-type"
                                                    onClick={this.handleImageRemove}
                                                />
                                                <label htmlFor="fileRemove4"><i className="fa fa-trash-o"></i></label>
                                            </div>
                                        </div>
                                        <Form.Control id="txtLabel4"
                                            value={this.state.Caption4}
                                            className="text-center"
                                            onChange={this.onLabelChange}
                                            placeholder="Image Caption"
                                            required />
                                    </Form.Group>
                                </Col>

                                <Col lg={6}>
                                    <Form.Group>
                                        <div className="image-upload-container image-upload--small">
                                            <img alt='Loading' src={this.state.Image5 || require("../assets/images/defaultuploaderimg.png")} />
                                            <div className="image-upload-overlay"></div>
                                            <div className="image-upload-file-container">
                                                <input id="fileImg5"
                                                    type="file"
                                                    className="custom-file-type"
                                                    onChange={this.changeSelectedPhoto}
                                                />
                                                <label htmlFor="fileImg5"><i className="fa fa-upload"></i></label>
                                            </div>
                                            <div className="image-delete-file-container">
                                                <input id="fileRemove5"
                                                    type="button"
                                                    className="custom-file-type"
                                                    onClick={this.handleImageRemove}
                                                />
                                                <label htmlFor="fileRemove5"><i className="fa fa-trash-o"></i></label>
                                            </div>
                                        </div>
                                        <Form.Control id="txtLabel5"
                                            value={this.state.Caption5}
                                            className="text-center"
                                            onChange={this.onLabelChange}
                                            placeholder="Image Caption"
                                            required />
                                    </Form.Group>
                                </Col>
                            </Row>
                        </Col>

                        <Col lg={7}>
                            <Editor
                                apiKey="API_KEY"
                                init={{
                                    plugins: 'link table',
                                    height: 500,
                                    statusbar: false
                                }}
                                initialValue={this.state.Body}
                                onChange={this.handleEditorChange}
                            />
                        </Col>
                    </Row>
                    <br />
                </Container>
            </React.Fragment>
        );
    }
}