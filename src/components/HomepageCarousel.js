import React from 'react';

import Slider from 'react-slick';
import { Link } from 'react-router-dom';
import { config } from '../environment/environment';
import { getFilesForSlider } from '../sp-data/lists/list-content';

import '../styles/_homepage.scss';

const PreviousButton = (props) => {
  const { className, style, onClick } = props;
  return (
    <button onClick={onClick} style={{ zIndex: 5 }} type='button' className={`slick-btn slick-prev`}><i className='fa fa-angle-left' aria-hidden='true' style={{ marginLeft: "-2px", marginTop: '-2px' }}></i></button>
  );
}

const NextButton = (props) => {
  const { className, style, onClick } = props;
  return (
    <button onClick={onClick} type='button' className={`slick-btn slick-next`} ><i style={{ marginLeft: "3px", marginTop: '-2px' }} className='fa fa-angle-right' aria-hidden='true'></i></button>
  );
}

class HomepageCarousel extends React.Component {
  state = {
    carouselPictures: [],
    retrievalStatus: false
  }

  slideReroute = (id) => {
    this.props.history.push(`/Whats New/ViewItem/${id}`);
    //console.log(this.props);
  }

  componentDidMount() {
    const carouselPictures = [];
    getFilesForSlider().then(result => {
      result.forEach(item => {
        carouselPictures.push(
          {
            imageUrl: config.meralcoUrl + "/" + item.url,
            id: item.id
          }
        );
      });
      this.setState((prevState) => {
        return {
          carouselPictures,
          retrievalStatus: true
        }
      });
    })
  }
  render() {
    console.log(this.state.homepagePictures);
    return (
      <div>
        {
          this.state.retrievalStatus ?
            <Slider
              dots={false}
              prevArrow={<PreviousButton />}
              nextArrow={<NextButton />}
              slidesToShow={1}
              slidesToScroll={1}
              fade={true}
              cssEase={'linear'}
              className={'carousel-homepage'}
              infinite={false}
            >
              {
                this.state.carouselPictures.length !== 0 ? this.state.carouselPictures.map((item, index) => {
                  var itemURL = encodeURI(item.imageUrl).replace("(", "%28").replace(")", "%29").replace(/'/g, "%27");
                  return (
                    <div className="carousel-slides" key={index}>           
                      <div onClick={() => {this.slideReroute(item.id)}} spid={item.id} className="carousel-slide" style={
                        { 
                          backgroundImage: `url(${itemURL})`,
                          cursor: 'pointer'
                        }
                      }>
                      </div>                    
                    </div>
                  )
                }) :
                  <div>Nothing!</div>
              }
            </Slider>
            :
            <div>Loading</div>
        }
      </div>
    );
  }
}

export default HomepageCarousel;