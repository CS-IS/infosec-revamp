import React from 'react';

import { getItemById, addItemToList, updateItemViews, getAcknowledgedUndertaking } from "../sp-data/lists/list-content";

import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Button from 'react-bootstrap/Button';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { isAdmin, currentUser } from '../sp-data/spPermissions';
import { withRouter } from "react-router";
import Swal from 'sweetalert2';
import CountTo from 'react-count-to';

import Loader from './Loader';

import { config } from '../environment/environment';

import '../styles/_aboutpage.scss'
import '../styles/_global.scss';
import '../styles/_viewform.scss';

class ViewItem extends React.Component {
    state = {
        itemTitle: "",
        itemDetails: "",
        itemViews: 0,
        isAdmin: false,
        isLoading: true,
        currentUser: "",
        office: "",
        isAcknowledged: false
    }

    componentWillMount() {
        const { id, list } = this.props;

        isAdmin().then((result) => {
            this.setState(() => ({
                isAdmin: result
            }));
        });

        currentUser().then((result) => {
            debugger;
            this.setState({
                currentUser: result.name,
                office : result.office
            });

            getAcknowledgedUndertaking("Acknowledgements", this.state.currentUser, id).then((result) => {
                if (result.length > 0) {
                    this.setState({
                        isAcknowledged: true
                    })
                }
            });
        });

        getItemById(id, list).then((data) => {
            this.setState(() => ({
                itemTitle: data.Title,
                itemDetails: data.Details,
                itemViews: data.Views,
                isLoading: false
            }));

            updateItemViews(list, id, this.state.itemViews).then((data) => {
                console.log("Updated views");
            });
        });
    }

    formattedDate = (d = new Date) => {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        var day = d.getDate();
        var monthIndex = d.getMonth();
        var year = d.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }

    handleAcknowledgement = () => {
        let data = {};
        data = {
            Title: this.state.currentUser,
            UndertakingId: this.props.id,
            Office:this.state.office,
            UndertakingTitle: this.state.itemTitle
        }

        this.setState({
            isAcknowledged: true
        });

        addItemToList("Acknowledgements", data).then((item) => {
            console.log(item);
        });
    }

    render() {
        return (
            <React.Fragment>
                <Container className={"view-form form-background with-padding padding-left"}>
                    {this.state.isLoading ?
                        <div style={{ marginLeft: '-35px', height: '58%' }}><Loader /></div> :
                        (<React.Fragment>
                            {
                                <Row className="view-item-row" style={{ display: this.state.isAcknowledged ? "block" : "none" }}>
                                    <div className="alert alert-secondary alert-dismissible fade show">
                                    <i class="fa fa-info-circle"></i> You already acknowledged the undertaking.
                                    </div>
                                </Row>
                            }
                            <Row className="view-item-row">
                                <Col md={12}>
                                    <h3 style={{ "textAlign": "center" }}>
                                        {this.state.itemTitle}
                                    </h3>
                                </Col>
                            </Row>

                            <Row className="view-item-row">
                                <Col md={12} dangerouslySetInnerHTML={{ __html: this.state.itemDetails }}></Col>

                                <Col md={12} style={{ "textAlign": "center" }}>
                                    <br />
                                    {this.formattedDate(new Date())}
                                    <br />
                                    <br />
                                    <br />
                                    {this.state.currentUser}
                                    <br />
                                    Printed Name and Signature
                                <br />
                                    <br />
                                    <br />
                                    <div className="button-center">
                                        <ButtonToolbar>
                                            <Button variant="light"
                                                onClick={this.handleAcknowledgement}
                                                disabled={this.state.isAcknowledged}>
                                                I Acknowledge
                                        </Button>
                                        </ButtonToolbar>
                                    </div>
                                </Col>
                            </Row>

                            <br />
                            <br />
                        </React.Fragment>)}
                </Container>
                <div className={"view-item-count"}>
                    <i className="ms-Icon ms-Icon--View" aria-hidden="true"></i>
                    &nbsp;<CountTo to={this.state.itemViews} speed={1234} /> Views
                </div>
                <br />
                <br />
                <br />
                <br />
            </React.Fragment>
        )
    }

}

export default withRouter(ViewItem);