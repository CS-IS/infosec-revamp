import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import InnerHeader from './InnerHeader';
import ListForm from './ListForm';
import Row from 'react-bootstrap/Row';
import ViewControl from './ViewControl';

import { getItemById, getFilesInsideFolder } from '../sp-data/lists/list-content';
import { updateListItems, deleteAttachments, addFile, updateListItemsThumbnail } from '../sp-data/lists/list-item';
import { config } from '../environment/environment';

import '../styles/_aboutpage.scss'
import '../styles/_global.scss';

export default class EditForm extends React.Component {
    state = {
        title: "",
        dateReleased: "",
        details: "",
        ItemType: "",
        category: "",
        thumbnail: "",
        videoEmbed:"",
        listAttachmentsLink: {
            fileAttachmentLink: "",
            thumbnailAttachmentLink: "",
        },
        dataRetrieval: false,
        originalFilename : ""
    }

    componentDidMount() {
        const { id, list } = this.props.match.params;
         getItemById(id, list).then((data) => {
    
            getFilesInsideFolder(list, id).then((fileData) => {
                const productionUrl = config.meralcoUrl;
                const fileAttachments = fileData.Files;
                var thumbnailUrl = "";
                var attachmentUrl = "";
                var attachmentName = "";
                fileAttachments.results.forEach(function (fileAttachment) {
                    const fileType = fileAttachment.ListItemAllFields.FileType;
                    const fileUrl = fileAttachment.ServerRelativeUrl;
    
                    if (fileType === "Thumbnail") {
                       
                        thumbnailUrl = fileUrl;
                      
                    } else if (fileType === "Attachment") {
                        
                        attachmentUrl = fileUrl;
                        attachmentName = fileAttachment.Name;                   
                    }
                });
                debugger;
                this.setState((prevstate) => ({
                    ...prevstate,       
                    title: data.Title ,
                    originalFilename : attachmentName,
                    dateReleased: new Date(data.DateReleased),
                    details: data.Details,
                    videoEmbed:data.VideoLink,
                    ItemType: data.ItemType ,
                    category: data.Category,
                    thumbnail: data.Thumbnail,
                    dataRetrieval: true,
                    listAttachmentsLink: {
                        fileAttachmentLink: attachmentUrl === "" ? "" : productionUrl + attachmentUrl,
                        thumbnailAttachmentLink: thumbnailUrl === "" ? "" : productionUrl + thumbnailUrl
                    }
                }));
                
                console.log(this.state);
            
            });
        });   
    }

    delayGoBack = (attachment) => {
        if(!attachment){
            setTimeout(this.props.history.goBack,2000)
        }
    }    
    
    handleAddClick = async (list ,category, data) => {
        debugger;
        const ID = this.props.match.params.id;
        const updateItemStatus = await updateListItems(list, ID, data).then(()=>true).catch(()=>false);
        const newFileAttachment = data.listAttachments.fileAttachment;
        const newThumbnail = data.listAttachments.thumbnailAttachment;
        const oldFileAttachmentLink = data.listAttachmentsLink.fileAttachmentLink;
        const oldThumbnailLink = data.listAttachmentsLink.thumbnailAttachmentLink;

        if(updateItemStatus){            
            if(newFileAttachment === null && newThumbnail === null){
                //No New Attachment, Nothing To Save, Delete, or Upload
                setTimeout(this.props.history.goBack,2000);
            }else if(newFileAttachment !== null && newThumbnail !== null){
                //Both Has New Attachment
                const oldFileAttachmentDeletionStatus = await deleteAttachments(list, ID, oldFileAttachmentLink);
                const oldThumnailDeletionStatus = await deleteAttachments(list, ID, oldThumbnailLink);
                const addNewFileAttachmentStatus = await addFile(list, newFileAttachment, ID, 'Attachment');
                const addThumbnailStatus = await addFile(list, newThumbnail, ID, 'Thumbnail');
                const updateThumbnailFieldStatus = await updateListItemsThumbnail(list, ID, newThumbnail.name);
                setTimeout(this.props.history.goBack,2000);
            }else if(newFileAttachment){
                //New File Attachment
                const oldFileAttachmentDeletionStatus = await deleteAttachments(list, ID, oldFileAttachmentLink);
                const addNewFileAttachmentStatus = await addFile(list, newFileAttachment, ID, 'Attachment');
                setTimeout(this.props.history.goBack,2000);
            }else if(newThumbnail){
                //New Thumbnail Attachment
                const oldThumnailDeletionStatus = await deleteAttachments(list, ID, oldThumbnailLink);
                const addThumbnailStatus = await addFile(list, newThumbnail, ID, 'Thumbnail');
                const updateThumbnailFieldStatus = await updateListItemsThumbnail(list, ID, newThumbnail.name);
                setTimeout(this.props.history.goBack,2000);
            }else{
                console.log("No match");
            }
        }
    }
    
    render() {
        console.log(this.state);
        let innerHeadertext = "";
        
        if(this.props.match.params.category){
            if(this.props.match.params.list === this.props.match.params.category){
                innerHeadertext = `${this.props.match.params.list} > Edit Item`;
            }else{
                innerHeadertext = `${this.props.match.params.list} > ${this.props.match.params.category} > Edit Item`;
            }
        }else{
            innerHeadertext = `${this.props.match.params.list} > Edit Item`;
        }
        return (
            <div className="about-page">
                <div className="about-page-container">
                    <Container fluid="true" className={"no-padding"}>
                        <Row noGutters="true">
                            <Col lg={2}>
                                <ViewControl isDisabled={true}/>
                            </Col>
                            <Col lg={10}>
                                <InnerHeader text = {innerHeadertext} />
                                <ListForm 
                                list={this.props.match.params.list}
                                category={this.props.match.params.category}
                                handleAddClick={this.handleAddClick}
                                data={this.state}
                                />
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}