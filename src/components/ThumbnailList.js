import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';
import Row from 'react-bootstrap/Row';

import '../styles/_thumbnail.scss'

class ThumbnailList extends React.Component {
    render() {
        const data = this.props.data;
        console.log(this.props);
        return (
            <div className="thumbnail-list-container">
                <Container className="thumbnail-list">
                    {
                        data.map((item, index) => {
                            /* return <Col key={index} lg={4}>
                                <div className="thumbnail-card">
                                    <div className="thumbnail-card-image">
                                        <Link to={(!this.props.category ? this.props.list : this.props.category) + "/ViewItem/" + item.id}>
                                            {
                                                item.thumbnail ? <img src={item.thumbnail} /> : <img src={require("../assets/images/logo.png")} />
                                            }
                                        </Link>
                                    </div>
                                    <div className="thumbnail-card-title">
                                        <h5>{item.title}</h5>
                                    </div>
                                    <div className="thumbnail-card-content">
                                        <p className="thumbnail-card-content-date">{item.year ? Math.round(item.year) : null}</p>
                                        <div className="thumbnail-card-content-action">
                                        </div>
                                    </div>
                                </div>
                            </Col> */

                            return <div className="thumbnail-card">
                                <div className="img-container">
                                    <Link to={(!this.props.category ? this.props.list : this.props.category) + "/ViewItem/" + item.id}>
                                        {
                                            item.thumbnail ?
                                                <img src={item.thumbnail} />
                                                :
                                                <img
                                                    src={
                                                        item.title === "Use of Company - Issued Mobile Devices" ?
                                                            require("../assets/images/Use of Company Issued Mobile Device.jpg")
                                                            :
                                                            item.title === "Use of Personally - Owned Devices" ?
                                                                require("../assets/images/Use of Personally Owned Mobile Device.jpg")
                                                                :
                                                                require("../assets/images/black.jpg")
                                                    } />
                                        }
                                    </Link>
                                </div>
                                <div className="content-container">
                                    <h5>{item.title}</h5>
                                    <span>{item.year ? Math.round(item.year) : null}</span>
                                </div>
                            </div>
                        })
                    }
                </Container>
            </div>
        );
    }
}

export default ThumbnailList;