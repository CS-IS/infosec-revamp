import React from 'react';

import Slider from 'react-slick';

import { getHomepagePictures, LIST_ATTACHMENTS_PREFIX } from '../sp-data/lists/homepage-pictures';

import '../styles/_homepage.scss';

class BackgroundSlider extends React.Component {
    state = {
        homepagePictures: [],
        retrievalStatus: false
    }

    componentDidMount() {
        getHomepagePictures().then((result) => {
            if (result.length > 0) {
                console.log(result);
                const homepagePictures = [];

                result = this.shuffle(result);

                result.forEach((item) => {
                    homepagePictures.push(
                        {
                            imageUrl: item.ImageUrl,
                            Id: item.Id
                        }
                    );
                });

                this.setState((prevState) => {
                    return {
                        homepagePictures,
                        retrievalStatus: true
                    }
                });
            }
        });
    }

    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    render() {
        console.log(this.state.homepagePictures);
        return (
            <div>
                {
                    this.state.homepagePictures.length > 0 ?
                        <Slider
                            dots={false}
                            infinite={true}
                            autoplay={true}
                            autoplaySpeed={10000}
                            speed={500}
                            slidesToShow={1}
                            slidesToScroll={1}
                            fade={true}
                            cssEase={'linear'}
                        >
                            {
                                this.state.homepagePictures.map((item, index) => {
                                    return (
                                        <img key={index} src={`${LIST_ATTACHMENTS_PREFIX}/${item.Id}/${item.imageUrl}`} />
                                    )
                                })
                            }
                        </Slider>
                        :
                        <div>Loading</div>
                }
            </div>
        );
    }
}

export default BackgroundSlider;