import React from 'react';

import { getItemById, getFilesInsideFolder, updateItemViews } from "../sp-data/lists/list-content";

import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import CountTo from 'react-count-to';
import Loader from './Loader';
import Row from 'react-bootstrap/Row';
import Swal from 'sweetalert2';

import { config } from '../environment/environment';
import { isAdmin } from '../sp-data/spPermissions';
import { deleteListItem, deleteFolder } from '../sp-data/lists/list-item';
import { withRouter } from "react-router";

import '../styles/_aboutpage.scss';
import '../styles/_global.scss';
import '../styles/_viewform.scss';

class ViewItem extends React.Component {
    state = {
        itemTitle: "",
        itemType: "Text",
        category: "",
        itemDetails: "",
        itemThumbnail: "",
        hasAttachment: false,
        itemAttachment: "",
        itemAttachmentName: "",
        itemViews: 0,
        itemVideo: "",
        created: "",
        createdBy: "",
        modified: "",
        lastModified: "",
        showDetails: false,
        showAttachment: false,
        showThumbnail: false,
        showVideo: false,
        isAdmin : false,
        isLoading : true
    }

    changeFileType(filetype) {
        switch (filetype) {
            case "Text":
                this.setState({
                    showDetails: true,
                    showAttachment: false,
                    showThumbnail: true,
                    showVideo: false
                });
                break;
            case "PDF":
                this.setState({
                    showDetails: false,
                    showAttachment: true,
                    showThumbnail: true,
                    showVideo: false
                });
                break;
            case "PowerPoint":
                this.setState({
                    showDetails: false,
                    showAttachment: true,
                    showThumbnail: true,
                    showVideo: false
                });
                break;
            case "Image":
                this.setState({
                    showDetails: true,
                    showAttachment: true,
                    showThumbnail: false,
                    showVideo: false
                });
                break;
            case "Video":
                this.setState({
                    showDetails: false,
                    showAttachment: false,
                    showThumbnail: true,
                    showVideo: true
                });
                break;
            default:
                this.setState({
                    ...this.state
                });
                break;
        }
    }

    componentWillMount() {
        
        isAdmin().then((result) => {
            this.setState(()=>({
                isAdmin : result
            }));
        });

        const { id, list } = this.props;
        getItemById(id, list).then((data) => {
            switch (data.ItemType) {
                case "Text":
                    this.setState(() => ({
                        itemTitle: data.Title,
                        itemStartDate: data.DateReleased,
                        itemDetails: data.Details,
                        itemType: data.ItemType,
                        itemViews: data.Views
                    }));
                    break;
                case "PDF":
                    this.setState({
                        itemTitle: data.Title,
                        itemStartDate: data.DateReleased,
                        itemDetails: data.Details,
                        itemType: data.ItemType,
                        itemViews: data.Views || 0
                    });
                    break;
                case "PowerPoint":
                    this.setState({
                        itemTitle: data.Title,
                        itemStartDate: data.DateReleased,
                        itemDetails: data.Details,
                        itemType: data.ItemType,
                        itemViews: data.Views
                    });
                    break;
                case "Image":
                    this.setState({
                        itemTitle: data.Title,
                        itemStartDate: data.DateReleased,
                        itemDetails: data.Details,
                        itemType: data.ItemType,
                        itemViews: data.Views || 0
                    });
                    break;
                case "Video":
                    this.setState({
                        itemTitle: data.Title,
                        itemStartDate: data.DateReleased,
                        itemDetails: data.Details,
                        itemType: data.ItemType,
                        itemVideo: data.VideoLink || "",
                        itemViews: data.Views
                    });
                    break;
                default:
                    this.setState({
                        ...this.state
                    });
                    break;
            }

            if (this.state.category === "Awareness") {
                this.changeFileType("Image");
            } else {
                this.changeFileType(this.state.itemType);
            }

            updateItemViews(list, id, this.state.itemViews).then((data)=>{
                getFilesInsideFolder(list, id).then((data) => {
                    const productionUrl = config.meralcoUrl;
                    const fileAttachments = data.Files;
                    var thumbnailUrl = "";
                    var attachmentUrl = "";
                    var attachmentName = "";
                    fileAttachments.results.forEach(function (fileAttachment) {
                        const fileType = fileAttachment.ListItemAllFields.FileType;
                        const fileUrl = fileAttachment.ServerRelativeUrl;
        
                        if (fileType === "Thumbnail") {
                            thumbnailUrl = fileUrl;
                          
                        } else if (fileType === "Attachment") {
                            attachmentUrl = fileUrl;
                            attachmentName = fileAttachment.Name;                   
                        }
                    });
                    
                    debugger;
                    attachmentUrl = attachmentUrl ? productionUrl + attachmentUrl : "";
                    thumbnailUrl = thumbnailUrl ? productionUrl + thumbnailUrl : "";
                    if (this.state.category === "Awareness") {
                        this.setState({
                            itemAttachment: attachmentUrl,
                            itemThumbnail: thumbnailUrl,
                            itemAttachmentName: attachmentName,
                            isLoading : false
                        });
                    }else{
                        this.setState({
                            itemAttachment: attachmentUrl,
                            itemThumbnail: thumbnailUrl,
                            itemAttachmentName: attachmentName,
                            isLoading : false
                        });
                    }
                });
            });            
        });
    }

    formattedDate = (d = new Date) => {
        let month = String(d.getMonth() + 1);
        let day = String(d.getDate());
        const year = String(d.getFullYear());
      
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
      
        return `${month}/${day}/${year}`;
      }
    handleBackClick = () => {
        this.props.history.goBack();
    }

    handleDeleteClick = () => {
        const id = this.props.match.params.id;
        const list = this.props.match.params.list;
        Swal.fire({
            title: 'Are you sure you want to delete this item?',
            showCancelButton: true,
            confirmButtonColor: '#3cb371',
            cancelButtonColor: '#dc143c',
            confirmButtonText: 'Yes',
            cancelButtonText:'No'
          }).then((result) => {
            if (result.value) {
                this.setState(() => ({
                    dataSaving: true
                }));
                deleteListItem(list, id).then(() => {
                    deleteFolder(list, id).then(() => {
                        this.props.history.goBack();
                    });
                });
                
            }
        });        
    }

    handleEditClick = () => {
        let path = '';
        const { id, list, category } = this.props.match.params;
        if (category) {
            path = `/${list}/${category}/EditItem/${id}`;
        } else {
            path = `/${list}/EditItem/${id}`;
        }
        this.props.history.push(path);
    }


    render() {
        const startDate = new Date(this.state.itemStartDate);
        return (
            <React.Fragment>
                <Container className={"view-form form-background with-padding padding-left"} id="customScrollbar">
                {this.state.isLoading ? <div style={{marginLeft: '-35px',height: '58%'}}><Loader/></div> : (<React.Fragment><Row className="view-item-row">
                        <Col md={2}>Title</Col>
                        <Col md={10}>{this.state.itemTitle}
                        </Col>
                    </Row>

                    <Row className="view-item-row">
                        <Col md={2}>Date Released</Col>
                        <Col md={10}>{
                            this.formattedDate(startDate)
                        }</Col>
                    </Row>

                    <Row className="view-item-row">
                        <Col md={2}>Type</Col>
                        <Col md={10}>{this.state.itemType}</Col>
                    </Row>

                    <Row className="view-item-row"
                        style={{ display: this.state.showDetails ? '' : 'none' }}>
                        <Col md={2}>Details</Col>
                        <Col md={10} dangerouslySetInnerHTML={{ __html: this.state.itemDetails }}></Col>
                    </Row>

                    <Row className="view-item-row" style={{ display: this.state.showAttachment ? 'block' : 'none' }}>
                        <Col md={2}>
                            {this.state.itemType === "Image" ? "Image" : "File Attachment"}
                        </Col>
                        <Col md={10}>
                            {
                                this.state.itemType === "Image" ?
                                this.state.itemThumbnail && <img src={this.state.itemThumbnail} width={"100%"} />
                                :                                
                                this.state.itemAttachment && <a href={this.state.itemAttachment + "?web=1"} target="_blank">{this.state.itemAttachmentName}</a>
                            }
                        </Col>
                    </Row>

                    <Row className="view-item-row" style={{ display: this.state.showVideo ? '' : 'none' }}>
                        <Col md={2}>Video</Col>
                        <Col md={10} dangerouslySetInnerHTML={{ __html: this.state.itemVideo }}></Col>
                    </Row>

                    <Row className="view-item-row" style={{ display: this.state.showThumbnail ? '' : 'none' }}>
                        <Col md={2}>Thumbnail</Col>
                        <Col md={10}>
                            {this.state.itemThumbnail && <img src={this.state.itemThumbnail} width={"200px"} height={"200px"} />}
                        </Col>
                    </Row>

                    <br />
                    <br />

                    <ButtonToolbar>
                        {this.state.isAdmin&&<Button variant="primary" onClick={this.handleEditClick} className="btn-infosec btn-infosec--edit">Edit</Button>}
                        {this.state.isAdmin&&<Button variant="primary" onClick={this.handleDeleteClick} className="btn-infosec btn-infosec--delete">Delete</Button>}
                        <Button variant="light" onClick={this.handleBackClick} className="btn-infosec btn-infosec--cancel">Back</Button>
                    </ButtonToolbar>
                </React.Fragment>)}
                </Container>
                <div className={"view-item-count"}>
                    <i className="ms-Icon ms-Icon--View" aria-hidden="true"></i>
                    &nbsp;<CountTo to={this.state.itemViews} speed={1234} /> Views
            </div>
            </React.Fragment>
        )
    }

}

export default withRouter(ViewItem);