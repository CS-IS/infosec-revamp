import React from 'react';

import '../styles/_global.scss'

class Footer extends React.Component {
    getYear() {
        return new Date().getFullYear();
    }
    
    render() {
        return (
            <div className="footer-container">
                <p>Meralco InfoSec <span>{this.getYear()}</span></p>
            </div>
        );
    }
}

export default Footer;