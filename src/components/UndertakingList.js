import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import InnerHeader from './InnerHeader';
import Loader from './Loader';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Row from 'react-bootstrap/Row';
import Spinner from 'react-bootstrap/Spinner';
import ThumbnailList from '../components/ThumbnailList';
import ViewControl from './ViewControl';

import { isAdmin } from '../sp-data/spPermissions';
import { withRouter } from "react-router";

import { getListContent, filterListContent } from "../sp-data/lists/list-content";

import '../styles/_aboutpage.scss';
import '../styles/_global.scss';

class List extends React.Component {

    state = {
        title: this.props.title || "Default title",
        category: this.props.category,
        items: [],
        selectedItems: [],
        itemsLoading: true,
        actions: [],
        listname: this.props.list,
        apiResult: {},
        loadMoreButton: false,
        isLoadingData: false,
        isLoadingData2: false,
        currentPath: "",
        itemCount: 0,
        isAdmin : false
    }

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }


    componentDidMount() {
        isAdmin().then((result) => {
            this.setState(()=>({
                isAdmin : result
            }));
        });
        this.setState(() => ({
            isLoadingData: true
        }));

        getListContent(this.state.listname, this.props.category).then((data) => {
            const listData = data.results.map((item) => {
                return {
                    id: item.Id,
                    thumbnail: item.Thumbnail,
                    title: item.Title,
                    year: item.Year
                }
            });

            this.setState(() => ({
                items: [...listData],
                apiResult: data,
                isLoadingData: false
            }));

            if (this.state.apiResult.hasNext || this.state.apiResult.nextUrl) {
                this.setState(() => ({
                    loadMoreButton: true
                }));
            }

        });

    }

    componentDidUpdate(prevProps) {
        if (prevProps.category !== this.props.category) {
            this.setState(() => ({
                category: this.props.category,
                currentPath: this.props.location.pathname,
                isLoadingData: true,
                loadMoreButton : false
            }));
            getListContent(this.state.listname, this.props.category).then((data) => {
                const listData = data.results.map((item) => {
                    return {
                        id: item.Id,
                        thumbnail: item.Thumbnail,
                        title: item.Title,
                        year: item.Year
                    }
                });
                this.setState(() => ({
                    items: [...listData],
                    apiResult : data,
                    isLoadingData: false
                }));
                if (this.state.apiResult.hasNext || this.state.apiResult.nextUrl) {
                    this.setState(() => ({
                        loadMoreButton: true
                    }));
                }
            });
        }
        this.scrollToBottom();
    }

    handleAdd = (e) => {
        e.preventDefault();
        let path = '';
        if (this.props.category) {
            path = `${this.props.category}/NewItem`;
        } else {
            path = `${this.props.title}/NewItem`;
        }
        this.props.history.push(path);
    }

    _onLoadMoreItems() {
        const { apiResult, items } = this.state;
        if (this.state.apiResult.hasNext || this.state.apiResult.nextUrl) {
            this.setState(() => ({
                isLoadingData2: true,
                loadMoreButton: false
            }))
            apiResult.getNext().then(api => {
                const listData = api.results.map((item) => {
                    return {
                        id: item.Id,
                        thumbnail: item.Thumbnail,
                        title: item.Title,
                        year: item.Year
                    }
                });

                this.setState(() => ({
                    items: [...items, ...listData],
                    apiResult: api,
                    loadMoreButton: api.hasNext || api.nextUrl ? true : false,
                    isLoadingData2: false
                }));
            });
        }
        else {
            this.setState({ loadMoreButton: false });
        }

    }

    _filterItem(title, year) {
        this.setState(() => ({
            isLoadingData: true
        }));
        if(title === '' && year === ''){
            getListContent(this.state.listname, this.props.category).then((data) => {
                const listData = data.results.map((item) => {
                    return {
                        id: item.Id,
                        thumbnail: item.Thumbnail,
                        title: item.Title,
                        year: item.Year
                    }
                });
    
                this.setState(() => ({
                    items: [...listData],
                    apiResult: data
                }));
    
                this.setState(() => ({
                    isLoadingData: false
                }));
    
                if (this.state.apiResult.hasNext || this.state.apiResult.nextUrl) {
                    this.setState(() => ({
                        loadMoreButton: true
                    }));
                }
    
            });
        }else{
            filterListContent(this.state.listname, this.state.category, title, year).then((data) => {
                const listData = data.results.map((item) => {   
                    return {
                        id: item.Id,
                        thumbnail: item.Thumbnail,
                        title: item.Title,
                        year: item.Year
                    }
                });
    
                this.setState(() => ({
                    items: [...listData],
                    loadMoreButton: false,
                    apiResult: data,
                    isLoadingData: false
                }));
                

                if (this.state.apiResult.hasNext || this.state.apiResult.nextUrl) {
                    this.setState(() => ({
                        loadMoreButton: true
                    }));
                }
    
            });    
        }

    }

    render() {
        const { items, currentPath } = this.state;

        let InnerHeaderText = "";
        if (this.props.category) {
            if (this.props.title === this.props.category) {
                InnerHeaderText = this.props.title;
            } else {
                InnerHeaderText = `${this.props.title} > ${this.props.category}`;
            }
        } else {
            InnerHeaderText = `${this.props.title}`
        }

        return (
            <div className="about-page"
                ref={(el) => { this.messagesEnd = el; }}
                id="customScrollbar">
                <div className="about-page-container">

                    <Container fluid="true" className={"no-padding"}>
                        <Row noGutters="true">
                            <Col lg={2}>
                                <ViewControl category={this.state.category} data={items} path={currentPath} list={this.props.list} onSubmit={(value) => {
                                    this._filterItem(value.titleFilter, value.yearFilter);
                                }} />
                            </Col>
                            <Col lg={10}>
                                <InnerHeader text={InnerHeaderText}
                                    add={this.state.items.length > 0 ? true : false}
                                    delete={false}
                                    deleteDisabled={true}
                                    handleAdd={this.handleAdd}
                                    list={this.props.list}
                                    export={this.props.export}
                                />
                                {this.state.isLoadingData &&
                                    <div className="content-overlay">
                                        <Loader />
                                    </div>
                                }
                                    <div className="about-page-content" id="customScrollbar">
                                        {this.state.items.length === 0 && this.state.isLoadingData === false ?
                                            <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center',height: '95%'}}>
                                                <div>
                                                    <div style={{ zIndex: 10, color: 'white', textAlign: 'center', width: '450px', fontSize: '18px' }}>
                                                        <p>{`There are no items to show in "${InnerHeaderText}". ${this.state.isAdmin ? "Do you want to create a new one?" : ""}`}</p>
                                                        {this.state.isAdmin && <button className='btn-infosec btn-infosec--add' 
                                                        onClick={this.handleAdd}>
                                                        Add New
                                                        </button>}
                                                    </div>
                                                </div>
                                            </div> :
                                            <div style={{paddingBottom: '10px'}}>
                                                <ThumbnailList data={this.state.items} list={this.props.list} category={this.props.category} />
                                                <div style={{marginBottom: '20px'}} className="btn-center" >
                                                    {this.state.loadMoreButton && <button className="btn-infosec btn-infosec--link" onClick={(e) => {
                                                        this._onLoadMoreItems();
                                                    }} >Load More...</button>}
                                               
                                                { this.state.isLoadingData2 && <Spinner animation="border" variant="primary" />}
                                                </div>
                                            </div>
                                        }
                                    </div>
                            </Col>

                        </Row>
                    </Container>
                </div>
            </div>

        );
    }
}

export default withRouter(List);