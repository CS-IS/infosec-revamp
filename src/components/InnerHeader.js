import React from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import { isAdmin } from '../sp-data/spPermissions';
import { links } from '../sp-data/sp-links';
import { getCountIfMaximum } from "../sp-data/lists/list-content";

import '../styles/_innerheader.scss';
import '../styles/_global.scss';
import '../styles/_buttons.scss';

class InnerHeader extends React.Component {
    state = {
        itemCount: 0,
        isAdmin : false
    }

    componentDidMount() {
        isAdmin().then((result) => {
            this.setState(()=>({
                isAdmin : result
            }));
        });        

        if (this.props.list === "Whats New") {
            getCountIfMaximum(this.props.list).then((data) => {
                this.setState({
                    itemCount: data.length
                });
                console.log(this.state.itemCount);
            });
        }
    }

    disableButton = (e) => {
        e.preventDefault();
        return false;
    }

    render() {
        return (
            <div className="inner-header-container">
                <Container className={"inner-header"}>
                    <Row style={{ height: '33px' }}>
                        <Col lg={7}>
                            {this.props.text ? <h2>{decodeURIComponent(this.props.text)}</h2> : <h2>Default Text</h2>}
                        </Col>
                        <Col lg={5}>
                            <div className="inner-header-actions">
                                {
                                    this.props.edit &&
                                    <button className="btn-infosec btn-infosec--edit">
                                        <i className="ms-Icon ms-Icon--RecycleBin" aria-hidden="true"></i>Edit
                                    </button>
                                }
                                {
                                    this.props.delete &&
                                    <button className="btn-infosec btn-infosec--delete">
                                        <i className="ms-Icon ms-Icon--RecycleBin" aria-hidden="true"></i>Delete
                                    </button>
                                }
                                {
                                    this.props.add && this.state.isAdmin &&
                                    <button
                                        className={`btn-infosec btn-infosec--add ${this.state.itemCount >= 5 ? "btn-infosec--disabled" : null}`}
                                        onClick={this.state.itemCount >= 5 ? this.disableButton : this.props.handleAdd}>
                                        Add New
                                    </button>

                                }
                                {
                                    this.props.export && this.state.isAdmin &&
                                    <a href={`${links.undertaking.acknowledgements}`} target="_blank">
                                    <button
                                        className={`btn-infosec btn-infosec--export ${this.state.itemCount >= 5 ? "btn-infosec--disabled" : null}`}
                                    >
                                        Export to Excel
                                    </button>
                                    </a>
                                }                                
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default InnerHeader;